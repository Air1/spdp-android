package com.ginger.museup.ui;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.ginger.museum.utils.LogUtils;

/**
 * Created by hungnguyendinhle on 5/11/15.
 */
public abstract class OnSwipeTouchListener implements View.OnTouchListener {

    private static final int SWIPE_DISTANCE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;


    float  lastX;
    float  lastY;

    private final GestureDetector gestureDetector;

    public OnSwipeTouchListener(Context context) {
        gestureDetector = new GestureDetector(context, new GestureListener());
    }

    public abstract void onSwipeLeft();
    public abstract void onSwipeRight();
    public abstract void onTap();

    @Override
    public boolean onTouch(View v, MotionEvent event) {

//        gestureDetector.onTouchEvent(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastX = event.getX();
                lastY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                float distanceX = event.getX() - lastX;
                float distanceY = event.getY() - lastY;
                LogUtils.d("Distance:", "" + distanceX);
                if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > SWIPE_DISTANCE_THRESHOLD) {
                    if (distanceX > 0) {
                        LogUtils.d("Distance:", "onSwipeRight: " + distanceX);
                        onSwipeRight();
                    } else {
                        LogUtils.d("Distance:", "onSwipeLeft: " + distanceX);
                        onSwipeLeft();
                    }
                    return true;
                } else {
                    LogUtils.d("Distance:", "onTap: " + distanceX);
                    onTap();
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
            default:
        }
        return true;
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {


        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            onTap();
            return super.onSingleTapUp(e);
        }

        private static final int SWIPE_DISTANCE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            Log.d("GestureListener", "onDown");
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            Log.d("GestureListener", "onFling");
            float distanceX = e2.getX() - e1.getX();
            float distanceY = e2.getY() - e1.getY();
            if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                if (distanceX > 0)
                    onSwipeRight();
                else
                    onSwipeLeft();
                return true;
            } else {
                onTap();
            }
            return false;
        }

        public GestureListener() {
            super();
        }
    }
}
