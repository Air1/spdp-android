package com.ginger.museup.ui;

import com.ginger.museum.R;
import com.ginger.museum.utils.LogUtils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView extends TextView {
	
	public static final int garagegothic = 0;
	public static final int GillSansStd = 1;
	public static final int GillSansStdBold = 2;
	public static final int GillSansStdBoldCondensed = 3;
	public static final int GillSansStdBoldExtraCond = 4;
	public static final int GillSansStdBoldItalic = 5;
	public static final int GillSansStdCondensed = 6;
	public static final int GillSansStdExtraBold = 7;
	public static final int GillSansStdExtraBoldDisp = 8;
	public static final int GillSansStdItalic = 9;
	public static final int GillSansStdLight = 10;
	public static final int GillSansStdLightItalic = 11;
	public static final int GillSansStdLightShadowed = 12;
	public static final int GillSansStdShadowed = 13;
	public static final int GillSansStdUltraBold = 14;
	public static final int GillSansStdUltraBoldCond = 15;
	
	int customFont = -1;
	
	public CustomTextView(Context context) {
		super(context);
		customFont = GillSansStd;
		setFont(customFont);
	}

	public CustomTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		TypedArray a = context.getTheme().obtainStyledAttributes(
		        attrs,
		        R.styleable.CustomTextView,
		        0, 0);
		try {
			customFont = a.getInteger(R.styleable.CustomTextView_custom_font_family, -1);
		} finally {
			a.recycle();
		}
		setFont(customFont);
		
	}

	public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		TypedArray a = context.getTheme().obtainStyledAttributes(
		        attrs,
		        R.styleable.CustomTextView,
		        0, 0);

		try {
			customFont = a.getInteger(R.styleable.CustomTextView_custom_font_family, -1);
		} finally {
			a.recycle();
		}
		setFont(customFont);
	}
	

	private void setFont(int fontFamily) {
		String fontLocation = null;
		switch (fontFamily) {
			case garagegothic:
				fontLocation = "fonts/garagegothic-regular.ttf";
				break;
			case GillSansStd:
				fontLocation = "fonts/GillSansStd.otf";
				break;
			case GillSansStdBold:
				fontLocation = "fonts/GillSansStd-Bold.otf";
				break;
			case GillSansStdBoldCondensed:
				fontLocation = "fonts/GillSansStd-BoldCondensed.otf";
				break;
			case GillSansStdBoldExtraCond:
				fontLocation = "fonts/GillSansStd-BoldExtraCond.otf";
				break;
			case GillSansStdBoldItalic:
				fontLocation = "fonts/GillSansStd-BoldItalic.otf";
				break;
			case GillSansStdCondensed:
				fontLocation = "fonts/GillSansStd-Condensed.otf";
				break;
			case GillSansStdExtraBold:
				fontLocation = "fonts/GillSansStd-ExtraBold.otf";
				break;
			case GillSansStdExtraBoldDisp:
				fontLocation = "fonts/GillSansStd-ExtraBoldDisp.otf";
				break;
			case GillSansStdItalic:
				fontLocation = "fonts/GillSansStd-Italic.otf";
				break;
			case GillSansStdLight:
				fontLocation = "fonts/GillSansStd-Light.otf";
				break;
			case GillSansStdLightItalic:
				fontLocation = "fonts/GillSansStd-LightItalic.otf";
				break;
			case GillSansStdLightShadowed:
				fontLocation = "fonts/GillSansStd-LightShadowed.otf";
				break;
			case GillSansStdShadowed:
				fontLocation = "fonts/GillSansStd-Shadowed.otf";
				break;
			case GillSansStdUltraBold:
				fontLocation = "fonts/GillSansStd-UltraBold.otf";
				break;
			case GillSansStdUltraBoldCond:
				fontLocation = "fonts/GillSansStd-UltraBoldCond.otf";
				break;
			default:
				break;

		}
		try {
			if (fontLocation != null) {
				Typeface font = Typeface.createFromAsset(getContext().getAssets(),
						fontLocation);
				setTypeface(font);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
}