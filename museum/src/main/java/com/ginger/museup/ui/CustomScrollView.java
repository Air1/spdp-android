package com.ginger.museup.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

import com.ginger.museum.utils.LogUtils;

public class CustomScrollView extends ScrollView {

	float lastTop;
	private OnScrollViewListener mOnScrollViewListener;

	public void setOnScrollViewListener(OnScrollViewListener l) {
	    this.mOnScrollViewListener = l;
	}
	public CustomScrollView(Context context) {
        super(context);
    }

    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    /*
     * (non-Javadoc)
     * @see android.view.View#onScrollChanged(int, int, int, int)
     */
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
    	if (mOnScrollViewListener != null) {
    		mOnScrollViewListener.onScrollChanged( this, l, t, oldl, oldt );
    	}
        super.onScrollChanged( l, t, oldl, oldt );
    }
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
    	if (ev.getAction() == MotionEvent.ACTION_DOWN) {
    		lastTop = ev.getY();
    	}
    	if (ev.getAction() == MotionEvent.ACTION_UP) {
    		if (ev.getY() < lastTop) {
    			if (mOnScrollViewListener != null) {
            		mOnScrollViewListener.onScrollDown();
            	}
    		}
    		if (ev.getY() > lastTop) {
    			if (mOnScrollViewListener != null) {
            		mOnScrollViewListener.onScrollTop();
            	}
    		}
    		lastTop = 0;
    	} else {
    		if (lastTop == 0) {
    			lastTop = ev.getY();
    		}
    	}
    	if (!isEnabled()) {
    		return true;
    	}
    	return super.onTouchEvent(ev);
    }
	/*
         *
         */
    public interface OnScrollViewListener {
        void onScrollChanged( CustomScrollView v, int l, int t, int oldl, int oldt );
        void onScrollTop();
        void onScrollDown();
    }
}
