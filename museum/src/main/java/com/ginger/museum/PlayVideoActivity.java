package com.ginger.museum;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.MediaController;
import android.widget.VideoView;

import com.ginger.museum.utils.LogUtils;


public class PlayVideoActivity extends Activity {
    public final static String LINK = "link";
    String link = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_play_video);
        VideoView videoView = (VideoView) findViewById(R.id.videoView);
        MediaController mc = new MediaController(this);
        mc.setAnchorView(videoView);
        mc.setMediaPlayer(videoView);
        if (getIntent() != null) {
            link = getIntent().getStringExtra(LINK);
            LogUtils.d(getClass().toString(), "Link Video: " + link);
        }
        Uri video = Uri.parse(link);
        videoView.setMediaController(mc);
        videoView.setVideoURI(video);
        videoView.start();

    }
}
