package com.ginger.museum.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.StorageUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

public class ImageUtils {
	static ImageUtils instance = new ImageUtils();
	public static DisplayImageOptions options;
	public ImageUtils() {
	}

	public  static void init(Context mContext) {
		// This configuration tuning is custom. You can tune every option, you
		// may tune some of them,
		// or you can create default configuration by
		// ImageLoaderConfiguration.createDefault(this);
		// method.
		options = new DisplayImageOptions.Builder().cacheInMemory(false)
				.cacheOnDisk(true).build();

		File cacheDir = StorageUtils.getCacheDirectory(mContext);
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				mContext).threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.diskCache(new UnlimitedDiscCache(cacheDir)) // default
				.diskCacheSize(50 * 1024 * 1024)
				.diskCacheFileCount(1000)
				.threadPoolSize(3)
				.memoryCache(new LruMemoryCache(2 * 1024 * 1024))
				.defaultDisplayImageOptions(options)
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(
				config);

	}
	/*
	 * 
	 */
	public static void loadImageView(final Context context, String url, ImageView imageView, final LoadCallBack callback) {
		ImageLoader.getInstance().displayImage(url, imageView, options, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view,
										  Bitmap loadedImage) {
				super.onLoadingComplete(imageUri, view, loadedImage);
				if (callback != null) {
					callback.onLoadCompleted(loadedImage);
				}
			}

			@Override
			public void onLoadingCancelled(String imageUri, View view) {
				// TODO Auto-generated method stub
				super.onLoadingCancelled(imageUri, view);
				if (callback != null) {
					callback.onLoadCompleted(null);
				}
			}

			@Override
			public void onLoadingFailed(String imageUri, View view,
										FailReason failReason) {
				// TODO Auto-generated method stub
				super.onLoadingFailed(imageUri, view, failReason);
				if (callback != null) {
					callback.onLoadCompleted(null);
				}
			}
		});
	}
	/*
	 *
	 */
	public static void displayImage(Context mContext, String url, ImageView imgView) {
		// TODO Auto-generated method stub
		ImageLoader.getInstance().displayImage(url, imgView, options, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view,
										  Bitmap loadedImage) {
				super.onLoadingComplete(imageUri, view, loadedImage);
			}
		});
	}
	/*
	 *
	 */
	public static void displayBackgroundImage(Context mContext, String url, final ImageView imgView, final int width, final int height) {
		// TODO Auto-generated method stub
		ImageLoader.getInstance().loadImage(url, options, new ImageLoadingListener() {
			@Override
			public void onLoadingStarted(String s, View view) {

			}

			@Override
			public void onLoadingFailed(String s, View view, FailReason failReason) {

			}

			@Override
			public void onLoadingComplete(String s, View view, Bitmap bitmap) {
				LogUtils.d("Width", width + "/" + height);
				bitmap = Bitmap.createScaledBitmap(bitmap, (int)(width * (678.0f/627.0f)), (int)(height * (1080.0f/900.0f)), true);
//				imgView.setBackgroundDrawable(new BitmapDrawable(bitmap));
				imgView.setImageBitmap(bitmap);
			}

			@Override
			public void onLoadingCancelled(String s, View view) {

			}
		});
	}
	/*
	 *
	 */
	public static void displayBackgroundImage(Context mContext, String url, final View imgView) {
		// TODO Auto-generated method stub
		ImageLoader.getInstance().loadImage(url, options, new ImageLoadingListener() {
			@Override
			public void onLoadingStarted(String s, View view) {

			}

			@Override
			public void onLoadingFailed(String s, View view, FailReason failReason) {

			}

			@Override
			public void onLoadingComplete(String s, View view, Bitmap bitmap) {
				imgView.setBackgroundDrawable(new BitmapDrawable(bitmap));
			}

			@Override
			public void onLoadingCancelled(String s, View view) {

			}
		});
	}
	public static interface LoadCallBack {
		void onLoadCompleted(Bitmap image);
	}
}
