package com.ginger.museum.utils;

import android.util.Log;

/**
 * Created by hungnguyendinhle on 5/8/15.
 */
public class LogUtils {

    public static void d(String tag, String name) {
        Log.d(tag, name);
    }
}
