package com.ginger.museum.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesUtils {

	private static String REFERENCE_NAME = "SharedPreferences";

	public static void savePreferenceBoolean(String key, Context myContext,
			boolean value) {
		SharedPreferences prefs = myContext.getSharedPreferences(
				REFERENCE_NAME, Context.MODE_PRIVATE);
		prefs.edit().putBoolean(key, value).commit();
	}

	public static boolean getPreferenceBoolean(String key, Context myContext,
			boolean value) {
		SharedPreferences prefs = myContext.getSharedPreferences(
				REFERENCE_NAME, Context.MODE_PRIVATE);
		return prefs.getBoolean(key, value);
	}

	public static void savePreferenceString(String key, Context myContext, String value) {
		SharedPreferences prefs = myContext.getSharedPreferences(
				REFERENCE_NAME, Context.MODE_PRIVATE);
		prefs.edit().putString(key, value).commit();
	}

	public static String getPreferenceString(String key, Context myContext,
			String value) {
		SharedPreferences prefs = myContext.getSharedPreferences(
				REFERENCE_NAME, Context.MODE_PRIVATE);
		return prefs.getString(key, value);
	}

	public static void savePreferenceInt(String key, Context myContext, int value) {
		SharedPreferences prefs = myContext.getSharedPreferences(
				REFERENCE_NAME, Context.MODE_PRIVATE);
		prefs.edit().putInt(key, value).commit();
	}

	public static int getPreferenceInt(String key, Context myContext, int value) {
		SharedPreferences prefs = myContext.getSharedPreferences(
				REFERENCE_NAME, Context.MODE_PRIVATE);
		return prefs.getInt(key, value);
	}

}
