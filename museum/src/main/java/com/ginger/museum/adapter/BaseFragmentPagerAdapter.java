package com.ginger.museum.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import com.ginger.museum.fragment.BaseFragment;
import java.util.List;

/**
 * Created by hungnguyendinhle on 4/27/15.
 */
public class BaseFragmentPagerAdapter extends FragmentPagerAdapter {

    List<BaseFragment> fragments;

    public BaseFragmentPagerAdapter(FragmentManager fm, List<BaseFragment> listFragments) {
        super(fm);
        fragments = listFragments;
    }

    @Override
    public Fragment getItem(int index) {
        return fragments.get(index);
    }

    @Override
    public int getCount() {
        return fragments != null ? fragments.size() : 0;
    }


    @Override
    public long getItemId(int position) {
        long itemID = fragments.get(position).getID();
        if (itemID == -1) {
            itemID =  position;
        }
        return itemID;
    }
}
