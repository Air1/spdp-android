package com.ginger.museum.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ginger.museum.R;
import com.ginger.museum.model.Artwork;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;
import com.ginger.museum.utils.UiUtils;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hungnguyendinhle on 04/26/15
 */
public class SlideshowAdapter extends BaseAdapter {

    List<Map<String, Object>> _list; //file, description
    Context _context;
    public SlideshowAdapter(Context context, List<Map<String, Object>> list) {
        _context = context;
        _list = list;
    }
    @Override
    public int getCount() {
        return _list != null ? _list.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return _list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        GalleryHolder holder = null;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(_context);
            view = inflater.inflate(R.layout.item_galerry, viewGroup, false);
            holder = new GalleryHolder();
            holder.imageArtwork = (ImageView)view.findViewById(R.id.imageArtwork);
            holder.subTitleView = (TextView)view.findViewById(R.id.textSubTitle);
            view.setTag(holder);
        } else {
            holder = (GalleryHolder)view.getTag();
        }
        Map<String, Object> artwork = _list.get(i);
        final ImageView imageView = holder.imageArtwork;
        String url = ((ParseFile)artwork.get("file")).getUrl();
        if ( url != null) {
            ImageUtils.loadImageView(_context, url, holder.imageArtwork, new ImageUtils.LoadCallBack() {
                @Override
                public void onLoadCompleted(Bitmap image) {
                    if (image != null) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                        params.width = UiUtils.getScreenWidth(_context);
                        params.height = (int) ((params.width * 1.0f / image.getWidth()) * image.getHeight());
                        imageView.setLayoutParams(params);
                    }
                }
            });
        }
        holder.subTitleView.setText(artwork.get("description").toString());
        return view;
    }
    /*
     *
     */
    static class GalleryHolder {
        ImageView imageArtwork;
        TextView subTitleView;
    }
}
