package com.ginger.museum.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ginger.museum.R;
import com.ginger.museum.model.Artwork;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;
import com.ginger.museum.utils.UiUtils;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by hungnguyendinhle on 04/26/15
 */
public class ArtworksAdapter extends BaseAdapter {

    List<Artwork> _list;
    Context _context;
    public ArtworksAdapter(Context context, List<Artwork> list) {
        _context = context;
        _list = list;
    }
    @Override
    public int getCount() {
        return _list != null ? _list.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return _list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ArtworkHolder holder = null;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(_context);
            view = inflater.inflate(R.layout.item_artwork, viewGroup, false);
            holder = new ArtworkHolder();
            holder.imageArtwork = (ImageView)view.findViewById(R.id.imageArtwork);
            holder.heartView = view.findViewById(R.id.heartView);
            holder.titleView = (TextView)view.findViewById(R.id.textTitle);
            holder.subTitleView = (TextView)view.findViewById(R.id.textSubTitle);
            holder.itemCuratorChoice = view.findViewById(R.id.itemCuratorChoice);
            view.setTag(holder);
        } else {
            holder = (ArtworkHolder)view.getTag();
        }
        Artwork artwork = _list.get(i);
        final ImageView imageView = holder.imageArtwork;
        if (artwork.getImage() != null) {
            ImageUtils.loadImageView(_context, artwork.getImage().getUrl(), holder.imageArtwork, new ImageUtils.LoadCallBack() {
                @Override
                public void onLoadCompleted(Bitmap image) {
                    if (image != null) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                        int width = UiUtils.getScreenWidth(_context);
                        width -= _context.getResources().getDimensionPixelSize(R.dimen.list_item_artwork_padding) * 4;
                        params.width = width / 2;
                        params.height = (int) ((params.width * 1.0f / image.getWidth()) * image.getHeight());
                        imageView.setLayoutParams(params);
                    }
                }
            });
        }
        holder.subTitleView.setText(artwork.getTitle());
        if (artwork.getExhibition() != null) {
            holder.titleView.setText(artwork.getExhibition().getName());
        }
        if (artwork.isFavorite(_context)) {
            holder.heartView.setVisibility(View.VISIBLE);
        } else {
            holder.heartView.setVisibility(View.INVISIBLE);
        }
        if (artwork.isCuratorChoice()) {
            holder.itemCuratorChoice.setVisibility(View.VISIBLE);
        } else {
            holder.itemCuratorChoice.setVisibility(View.INVISIBLE);
        }
        return view;
    }
    /*
     *
     */
    static class ArtworkHolder {
        ImageView imageArtwork;
        TextView titleView;
        TextView subTitleView;
        View heartView;
        View itemCuratorChoice;
    }
}
