package com.ginger.museum.model;

import com.ginger.museum.utils.LogUtils;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseRelation;

/**
 * Created by hungnguyendinhle on 5/10/15.
 */
@ParseClassName("Exhibition")
public class Exhibition extends BaseObject {

    boolean temporary;
    ParseFile cover;
    ParseFile sponsors;
    String name;
    String dates;
    String cardDescription;
    DidYouKnow didYouKnow;
    int order;
    String descriptionShort;
    String descriptionLong;
    String curratorStatement;
    String section1Title;
    String section1Description;
    Artwork section1Artwork; // (pointer to Artwork table)
    DidYouKnow section1DidYouKnow; // (pointer to DidYouKnow table) [optional]
    Artwork section2Artwork;// (pointer to Artwork table)
    DidYouKnow section2DidYouKnow;// (pointer to DidYouKnow table) [optional
    String section3Title;
    String section3Description;
    Artwork section3Artwork;// (pointer to Artwork table)
    DidYouKnow section3DidYouKnow;// (pointer to DidYouKnow table) [optional]
    ParseFile quoteBackground;// (image)
    String quoteText;// (string)
    String quoteAuthor;// (string)


    @Override
    public void parse() {
        for (String key : this.keySet()) {
            LogUtils.d("Exhibition data:", key + " : " + this.get(key) + "");
        }
        temporary = getBoolean("temporary");
        cover = getParseFile("cover");
        sponsors = getParseFile("sponsors");
        name = getString("name");
        dates = getString("dates");
        cardDescription = getString("cardDescription");
        didYouKnow = (DidYouKnow) getParseObject("didYouKnow");

        order = getInt("order");
        descriptionShort = getString("descriptionShort");
        descriptionLong = getString("descriptionLong");
        curratorStatement = getString("curratorStatement");

        section1Title = getString("section1Title");
        section1Description = getString("section1Desc");
        section1Artwork = (Artwork)getParseObject("section1Artwork");
        section1DidYouKnow = (DidYouKnow)getParseObject("section1DidYouKnow");

        section2Artwork = (Artwork)getParseObject("section2Artwork");
        section2DidYouKnow= (DidYouKnow)getParseObject("section2DidYouKnow");

        section3Title = getString("section3Title");
        section3Description = getString("section3Desc");
        section3Artwork = (Artwork)getParseObject("section3Artwork");
        section3DidYouKnow = (DidYouKnow)getParseObject("section3DidYouKnow");

        quoteText = getString("quoteText");
        quoteAuthor = getString("quoteAuthor");
        quoteBackground = getParseFile("quoteBackground");

        if (didYouKnow != null) {
            DataProvider.getInstance().getDidYouKnow(didYouKnow.getObjectId(), new GetCallback<DidYouKnow>() {
                @Override
                public void done(DidYouKnow object, ParseException e) {
                    didYouKnow = object;
                    if (didYouKnow != null) {
                        didYouKnow.parse();
                    }
                }
            });
        }
        if (section1Artwork != null) {
            DataProvider.getInstance().getArtwork(section1Artwork.getObjectId(), new GetCallback<Artwork>() {
                @Override
                public void done(Artwork object, ParseException e) {
                    section1Artwork = object;
                    if (section1Artwork != null) {
                        section1Artwork.parse();
                    }
                }
            });
        }
        if (section2Artwork != null) {
            DataProvider.getInstance().getArtwork(section2Artwork.getObjectId(), new GetCallback<Artwork>() {
                @Override
                public void done(Artwork object, ParseException e) {
                    section2Artwork = object;
                    if (section2Artwork != null) {
                        section2Artwork.parse();
                    }
                }
            });
        }
        if (section3Artwork != null) {
            DataProvider.getInstance().getArtwork(section3Artwork.getObjectId(), new GetCallback<Artwork>() {
                @Override
                public void done(Artwork object, ParseException e) {
                    section3Artwork = object;
                    if (section3Artwork != null) {
                        section3Artwork.parse();
                    }
                }
            });
        }
        if (section1DidYouKnow != null) {
            DataProvider.getInstance().getDidYouKnow(section1DidYouKnow.getObjectId(), new GetCallback<DidYouKnow>() {
                @Override
                public void done(DidYouKnow object, ParseException e) {
                    section1DidYouKnow = object;
                    if (section1DidYouKnow != null) {
                        section1DidYouKnow.parse();
                    }
                }
            });
        }
        if (section2DidYouKnow != null) {
            DataProvider.getInstance().getDidYouKnow(section2DidYouKnow.getObjectId(), new GetCallback<DidYouKnow>() {
                @Override
                public void done(DidYouKnow object, ParseException e) {
                    section2DidYouKnow = object;
                    if (section2DidYouKnow != null) {
                        section2DidYouKnow.parse();
                    }
                }
            });
        }
        if (section3DidYouKnow != null) {
            DataProvider.getInstance().getDidYouKnow(section3DidYouKnow.getObjectId(), new GetCallback<DidYouKnow>() {
                @Override
                public void done(DidYouKnow object, ParseException e) {
                    section3DidYouKnow = object;
                    if (section3DidYouKnow != null) {
                        section3DidYouKnow.parse();
                    }
                }
            });
        }
    }


    public boolean isTemporary() {
        return temporary;
    }

    public void setTemporary(boolean temporary) {
        this.temporary = temporary;
    }

    public ParseFile getCover() {
        return cover;
    }

    public void setCover(ParseFile cover) {
        this.cover = cover;
    }

    public ParseFile getSponsors() {
        return sponsors;
    }

    public void setSponsors(ParseFile sponsors) {
        this.sponsors = sponsors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    public String getCardDescription() {
        return cardDescription;
    }

    public void setCardDescription(String cardDescription) {
        this.cardDescription = cardDescription;
    }

    public DidYouKnow getDidYouKnow() {
        return didYouKnow;
    }

    public void setDidYouKnow(DidYouKnow didYouKnow) {
        this.didYouKnow = didYouKnow;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getDescriptionLong() {
        return descriptionLong;
    }

    public void setDescriptionLong(String descriptionLong) {
        this.descriptionLong = descriptionLong;
    }

    public String getCurratorStatement() {
        return curratorStatement;
    }

    public void setCurratorStatement(String curratorStatement) {
        this.curratorStatement = curratorStatement;
    }

    public String getSection1Title() {
        return section1Title;
    }

    public void setSection1Title(String section1Title) {
        this.section1Title = section1Title;
    }

    public String getSection1Description() {
        return section1Description;
    }

    public void setSection1Description(String section1Description) {
        this.section1Description = section1Description;
    }

    public Artwork getSection1Artwork() {
        return section1Artwork;
    }

    public void setSection1Artwork(Artwork section1Artwork) {
        this.section1Artwork = section1Artwork;
    }

    public DidYouKnow getSection1DidYouKnow() {
        return section1DidYouKnow;
    }

    public void setSection1DidYouKnow(DidYouKnow section1DidYouKnow) {
        this.section1DidYouKnow = section1DidYouKnow;
    }

    public Artwork getSection2Artwork() {
        return section2Artwork;
    }

    public void setSection2Artwork(Artwork section2Artwork) {
        this.section2Artwork = section2Artwork;
    }

    public DidYouKnow getSection2DidYouKnow() {
        return section2DidYouKnow;
    }

    public void setSection2DidYouKnow(DidYouKnow section2DidYouKnow) {
        this.section2DidYouKnow = section2DidYouKnow;
    }

    public String getSection3Title() {
        return section3Title;
    }

    public void setSection3Title(String section3Title) {
        this.section3Title = section3Title;
    }

    public String getSection3Description() {
        return section3Description;
    }

    public void setSection3Description(String section3Description) {
        this.section3Description = section3Description;
    }

    public Artwork getSection3Artwork() {
        return section3Artwork;
    }

    public void setSection3Artwork(Artwork section3Artwork) {
        this.section3Artwork = section3Artwork;
    }

    public DidYouKnow getSection3DidYouKnow() {
        return section3DidYouKnow;
    }

    public void setSection3DidYouKnow(DidYouKnow section3DidYouKnow) {
        this.section3DidYouKnow = section3DidYouKnow;
    }

    public ParseFile getQuoteBackground() {
        return quoteBackground;
    }

    public void setQuoteBackground(ParseFile quoteBackground) {
        this.quoteBackground = quoteBackground;
    }

    public String getQuoteText() {
        return quoteText;
    }

    public void setQuoteText(String quoteText) {
        this.quoteText = quoteText;
    }

    public String getQuoteAuthor() {
        return quoteAuthor;
    }

    public void setQuoteAuthor(String quoteAuthor) {
        this.quoteAuthor = quoteAuthor;
    }
}
