package com.ginger.museum.model;

import android.content.Context;

import com.ginger.museum.utils.LogUtils;
import com.ginger.museum.utils.PreferencesUtils;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hungnguyendinhle on 5/01/15.
 */
@ParseClassName("Artwork")
public class Artwork extends  BaseObject {

    ParseFile image;
    boolean isCuratorChoice;
    String title;
    String date;
    String description;
    Exhibition exhibition;
    DidYouKnow didYouKnow;
    List<Map<String, Object>> slideshow;
    @Override
    public void parse() {
        image = getParseFile("image");
        isCuratorChoice = getBoolean("curator_choice");
        title = getString("title");
        date = getString("date");
        description = getString("description");
        if (has("exhibition")) {
            exhibition = (Exhibition)getParseObject("exhibition");
            DataProvider.getInstance().getExhibition(exhibition.getObjectId(), new GetCallback<Exhibition>() {
                @Override
                public void done(Exhibition object, ParseException e) {
                    if (object != null) {
                        exhibition = object;
                    }
                }
            });
        }
        if (has("didYouKnow")) {
            didYouKnow = (DidYouKnow)getParseObject("didYouKnow");
            DataProvider.getInstance().getDidYouKnow(didYouKnow.getObjectId(), new GetCallback<DidYouKnow>() {
                @Override
                public void done(DidYouKnow object, ParseException e) {
                    didYouKnow = object;
                }
            });
        }
        if (has("slideshow")) {
            Map<String, Object> data = getMap("slideshow");
            slideshow = new ArrayList<Map<String, Object>>();
            for (String key : data.keySet()) {
                Map<String, Object> v = (Map<String, Object>)data.get(key);
                slideshow.add(v);
            }
        }
    }

    public ParseFile getImage() {
        return image;
    }

    public void setImage(ParseFile image) {
        this.image = image;
    }

    public boolean isCuratorChoice() {
        return isCuratorChoice;
    }

    public void setIsCuratorChoice(boolean isCuratorChoice) {
        this.isCuratorChoice = isCuratorChoice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Exhibition getExhibition() {
        return exhibition;
    }

    public void setExhibition(Exhibition exhibition) {
        this.exhibition = exhibition;
    }

    public DidYouKnow getDidYouKnow() {
        return didYouKnow;
    }

    public void setDidYouKnow(DidYouKnow didYouKnow) {
        this.didYouKnow = didYouKnow;
    }

    public boolean isFavorite(Context context) {
        return PreferencesUtils.getPreferenceBoolean("KEY_" + getObjectId(), context, false);
    }
    public void setFavorite(Context context, boolean isFavorite) {
        PreferencesUtils.savePreferenceBoolean("KEY_" + getObjectId(), context, isFavorite);
    }

    public List<Map<String, Object>> getSlideshow() {
        return slideshow;
    }

    public void setSlideshow(List<Map<String, Object>> slideshow) {
        this.slideshow = slideshow;
    }
}
