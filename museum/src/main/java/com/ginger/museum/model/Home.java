package com.ginger.museum.model;

import com.parse.ParseClassName;
import com.parse.ParseFile;

@ParseClassName("Home")
public class Home extends BaseObject {

	int order;
	ParseFile image;
	String title;
	String subTitle;
	String description;

	@Override
	public void parse() {
		order = getInt("order");
		title = getString("title");
		subTitle = getString("subTitle");
		description = getString("description");
		image = getParseFile("image");
	}

	public int getOrder() {
		return order;
	}

	public ParseFile getImage() {
		return image;
	}

	public String getTitle() {
		return title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public String getDescription() {
		return description;
	}
}
