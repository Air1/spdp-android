package com.ginger.museum.model;

import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;

/**
 * Created by hungnguyendinhle on 5/08/15.
 */
@ParseClassName("Museum")
public class Museum extends  BaseObject {

    String title;
    String subTitle;
    String description;
    String link;
    ParseFile image;
    DidYouKnow didYouKnow;
    @Override
    public void parse() {
        image = getParseFile("image");
        title = getString("title");
        subTitle = getString("subTitle");
        link = getString("link");
        description = getString("description");
        if (has("didYouKnow")) {
            didYouKnow = (DidYouKnow)getParseObject("didYouKnow");
        }
        if (didYouKnow != null) {
            DataProvider.getInstance().getDidYouKnow(didYouKnow.getObjectId(), new GetCallback<DidYouKnow>() {
                @Override
                public void done(DidYouKnow object, ParseException e) {
                    didYouKnow = object;
                }
            });
        }
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public ParseFile getImage() {
        return image;
    }

    public void setImage(ParseFile image) {
        this.image = image;
    }

    public DidYouKnow getDidYouKnow() {
        return didYouKnow;
    }

    public void setDidYouKnow(DidYouKnow didYouKnow) {
        this.didYouKnow = didYouKnow;
    }
}
