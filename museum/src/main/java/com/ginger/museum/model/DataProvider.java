package com.ginger.museum.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ginger.museum.R;
import com.ginger.museum.utils.LogUtils;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by hungndl on 5/8/15.
 */
public class DataProvider {
    public final static String TAG = DataProvider.class.toString();
    public final static String TABLE_HOME = "Home";
    public final static String TABLE_EXHIBITION = "Exhibition";
    public final static String TABLE_ARTWORK = "Artwork";
    public final static String TABLE_MUSEUM = "Museum";
    public final static String TABLE_DID_YOU_KNOW = "DidYouKnow";
    final String[] TABLES = new String[] {TABLE_EXHIBITION, TABLE_ARTWORK, TABLE_HOME, TABLE_DID_YOU_KNOW, TABLE_MUSEUM};

    static DataProvider instance = new DataProvider();
    int startSync = 0;
    boolean needToSync = false;
    String lastUpdateAt = "";
    Context mContext;
    public static DataProvider getInstance() {
        return instance;
    }

    public boolean isNeedToSync() {
        return needToSync;
    }

    /*
     *
     */
    public void init(Context context) {
        mContext = context;
        Parse.enableLocalDatastore(context);
        ParseObject.registerSubclass(Home.class);
        ParseObject.registerSubclass(Exhibition.class);
        ParseObject.registerSubclass(DidYouKnow.class);
        ParseObject.registerSubclass(Museum.class);
        ParseObject.registerSubclass(Artwork.class);
        Parse.initialize(context, context.getString(R.string.parse_app_key), context.getString(R.string.parse_client_key));
        lastUpdateAt = PreferenceManager.getDefaultSharedPreferences(mContext).getString("lastUpdateAt", "");
    }
    /*
     *
     */
    public void checkLastUpdate(final CallBack callBack) {
        //how to check last update
        ParseQuery<ParseObject> query = ParseQuery.getQuery(DataProvider.TABLE_EXHIBITION);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (list != null && e == null) {
                    LogUtils.d(TAG, "Total Records:" + list.size());
                    Date updateAt = null;
                    for (ParseObject object : list) {
                        LogUtils.d(TAG, "Total Records:" + object.getUpdatedAt());
                    }
                    for (ParseObject object : list) {
                        updateAt = object.getUpdatedAt();
                        break;
                    }
                    if (updateAt != null) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                        if (lastUpdateAt == null || lastUpdateAt.length() == 0) {
                            needToSync = true;
                        } else if (!lastUpdateAt.equalsIgnoreCase(simpleDateFormat.format(updateAt))) {
                            needToSync = true;
                        }
                        lastUpdateAt = simpleDateFormat.format(updateAt);
                    } else {
                        needToSync = true;
                    }
                }
                if (callBack != null) {
                    callBack.done();
                }
            }
        });
    }
    /*
     *
     */
    public void syncData(final Context mContext, final SyncCallBack syncCallBack) {
        //sync table home data

        int from = startSync;
        if (from == 0) {
            try {
                ParseObject.unpinAll();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
        String className = TABLES[from];
        syncTable(className, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                startSync++;
                LogUtils.d("Sync Data", "Index:" + startSync);
                if (e != null) {
                    LogUtils.d(TAG, "Sync table error:" + e.toString());
                }
                if (startSync == TABLES.length) {
                    needToSync = false;
                    //save lastupdateat
                    SharedPreferences setting = PreferenceManager.getDefaultSharedPreferences(mContext);
                    SharedPreferences.Editor editor = setting.edit();
                    editor.putString("lastUpdateAt", lastUpdateAt);
                    editor.commit();
                    if (syncCallBack != null) {
                        syncCallBack.done();
                    }
                } else {
                    if (syncCallBack != null) {
                        syncCallBack.onProgress(startSync, TABLES.length);
                    }
                    syncData(mContext, syncCallBack);
                }
            }
        });
    }
    /*
     *
     */
    public int getTotalTables() {
        return TABLES.length;
    }
    /*
     *
     */
    public void syncTable(final String tableName, final SaveCallback saveCallback) {
        //TODO: should check this line code: remove all data before sync
        try {
            ParseObject.unpinAll(tableName); //
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ParseQuery<ParseObject> query = ParseQuery.getQuery(tableName);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                LogUtils.d(TAG, "Total Records of " + tableName + ": " + list.size());
                //ParseObject.pinAllInBackground(list, saveCallback);
                ParseObject.pinAllInBackground(tableName, list, saveCallback);
            }
        });
    }
    /*
     *
     */
    public void getAllRecords(String tableName, FindCallback<ParseObject> findCallBack) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(tableName);
        query.fromLocalDatastore();
        query.findInBackground(findCallBack);
    }
    /*
     *
     */
    public void fetchHomeRecords(final FindCallback<Home> findCallBack) {
        ParseQuery<Home> query = ParseQuery.getQuery(TABLE_HOME);
        query.orderByAscending("order");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<Home>() {
            @Override
            public void done(List<Home> list, ParseException e) {
                for (Home item : list) {
                    item.parse();
                }
                if (findCallBack != null) {
                    findCallBack.done(list, e);
                }
            }
        });
    }
    /*
     *
     */
    public void fetchMuseumRecords(final FindCallback<Museum> findCallBack) {
        ParseQuery<Museum> query = ParseQuery.getQuery(TABLE_MUSEUM);
        query.orderByAscending("order");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<Museum>() {
            @Override
            public void done(List<Museum> list, ParseException e) {
                for (Museum item : list) {
                    item.parse();
                }
                if (findCallBack != null) {
                    findCallBack.done(list, e);
                }
            }
        });
    }
    /*
     *
     */
    public void fetchArtworkRecords(final FindCallback<Artwork> findCallBack) {
        ParseQuery<Artwork> query = ParseQuery.getQuery(TABLE_ARTWORK);
        query.orderByAscending("order");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<Artwork>() {
            @Override
            public void done(List<Artwork> list, ParseException e) {
                for (Artwork item : list) {
                    item.parse();
                }
                if (findCallBack != null) {
                    findCallBack.done(list, e);
                }
            }
        });
    }
    /*
     *
     */
    public void searchArtwork(final String keyword, final String exhibition, final FindCallback<Artwork> findCallBack) {
        ParseQuery<Artwork> query = ParseQuery.getQuery(TABLE_ARTWORK);
        query.orderByAscending("order");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<Artwork>() {
            @Override
            public void done(List<Artwork> list, ParseException e) {
                for (Artwork item : list) {
                    item.parse();
                }
                for (int i = list.size()-1; i >=0 ; i--) {
                    boolean filter = false;
                    if (exhibition != null && exhibition.length() > 0
                            && (list.get(i).getExhibition() == null
                        || !list.get(i).getExhibition().getObjectId().equalsIgnoreCase(exhibition))) {
                        filter = true;
                    }
                    if (keyword != null && keyword.length() > 0) {
                        if (!list.get(i).getTitle().toLowerCase().contains(keyword.toLowerCase())) {
                            filter = true;
                        }
                    }
                    if (filter) {
                        list.remove(i);
                    }
                }
                if (findCallBack != null) {
                    findCallBack.done(list, e);
                }
            }
        });
    }
    /*
     *
     */
    public void searchArtwork(final String keyword, final List<String> exhibitions, final FindCallback<Artwork> findCallBack) {
        ParseQuery<Artwork> query = ParseQuery.getQuery(TABLE_ARTWORK);
        query.orderByAscending("order");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<Artwork>() {
            @Override
            public void done(List<Artwork> list, ParseException e) {
                LogUtils.d("All items:", "" + list.size());
                for (Artwork item : list) {
                    item.parse();
                }
                for (int i = list.size()-1; i >=0 ; i--) {
                    boolean filter = false;
//                    if (exhibition != null && exhibition.length() > 0
//                            && (list.get(i).getExhibition() == null
//                            || !list.get(i).getExhibition().getObjectId().equalsIgnoreCase(exhibition))) {
//                        filter = true;
//                    }
                    if (!exhibitions.contains(list.get(i).getExhibition().getObjectId())) {
                        filter = true;
                    }
                    if (keyword != null && keyword.length() > 0) {
                        if (!list.get(i).getTitle().toLowerCase().contains(keyword.toLowerCase())) {
                            filter = true;
                        }
                    }
                    if (filter) {
                        list.remove(i);
                    }
                }
                if (findCallBack != null) {
                    findCallBack.done(list, e);
                }
            }
        });
    }

    /*
     *
     */
    public void fetchExhibitionRecords(final FindCallback<Exhibition> findCallback) {
        ParseQuery<Exhibition> query = ParseQuery.getQuery(TABLE_EXHIBITION);
//        query.orderByAscending("order");
        query.orderByDescending("order");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<Exhibition>() {
            @Override
            public void done(List<Exhibition> list, ParseException e) {
                for (Exhibition item : list) {
                    item.parse();
                }
                if (findCallback != null) {
                    findCallback.done(list, e);
                }
            }
        });
    }
    /*
     *
     */
    public void getDidYouKnow(String objectID, GetCallback<DidYouKnow> callBack) {
        ParseQuery<DidYouKnow> query = ParseQuery.getQuery(TABLE_DID_YOU_KNOW);
        query.fromLocalDatastore();
        query.getInBackground(objectID, callBack);
    }
    /*
     *
     */
    public void getExhibition(String objectID, GetCallback<Exhibition> callBack) {
        ParseQuery<Exhibition> query = ParseQuery.getQuery(TABLE_EXHIBITION);
        query.fromLocalDatastore();
        query.getInBackground(objectID, callBack);
    }
    /*
     *
     */
    public void getArtwork(String objectID, GetCallback<Artwork> callBack) {
        ParseQuery<Artwork> query = ParseQuery.getQuery(TABLE_ARTWORK);
        query.fromLocalDatastore();
        query.getInBackground(objectID, callBack);
    }
    /*
     *
     */
    public interface CallBack {
        void done();
    }
    public interface  SyncCallBack extends CallBack {
        void onProgress(int index, int total);
    }
}
