package com.ginger.museum.model;

import com.parse.ParseObject;

public abstract class BaseObject extends ParseObject{

    public abstract void parse();

}
