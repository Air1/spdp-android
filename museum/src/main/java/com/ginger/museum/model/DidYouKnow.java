package com.ginger.museum.model;

import com.parse.ParseClassName;
import com.parse.ParseFile;

/**
 * Created by hungnguyendinhle on 5/10/15.
 */
@ParseClassName("DidYouKnow")
public class DidYouKnow  extends  BaseObject {

    String title;
    String description;
    String link;
    ParseFile image;

    @Override
    public void parse() {
        image = getParseFile("image");
        title = getString("title");
        link = getString("link");
        description = getString("description");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public ParseFile getImage() {
        return image;
    }

    public void setImage(ParseFile image) {
        this.image = image;
    }
}