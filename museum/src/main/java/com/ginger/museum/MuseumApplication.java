package com.ginger.museum;

import android.app.Application;

import com.ginger.museum.model.DataProvider;
import com.ginger.museum.utils.ImageUtils;
import com.parse.Parse;

/**
 * Created by hungnguyendinhle on 5/8/15.
 */
public class MuseumApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DataProvider.getInstance().init(this);
        ImageUtils.init(this);
    }
}
