package com.ginger.museum;

import com.ginger.museum.fragment.AboutMuseumFragment;
import com.ginger.museum.fragment.ArtworkMainFragment;
import com.ginger.museum.fragment.BaseFragment;
import com.ginger.museum.fragment.CatalogueFragment;
import com.ginger.museum.fragment.DetectFragment;
import com.ginger.museum.fragment.DidYouKnowFragment;
import com.ginger.museum.fragment.ExhibitionDetailFragment;
import com.ginger.museum.fragment.ExhibitionHomeFragment;
import com.ginger.museum.fragment.HomeFragment;
import com.ginger.museum.fragment.LoadingFragment;
import com.ginger.museum.fragment.MapFragment;
import com.ginger.museum.fragment.MySpdpFragment;
import com.ginger.museum.fragment.OnNavigationListener;
import com.ginger.museum.fragment.QuizFragment;
import com.ginger.museum.fragment.WebBrowserFragment;
import com.ginger.museum.model.Artwork;
import com.ginger.museum.model.DataProvider;
import com.ginger.museum.model.DidYouKnow;
import com.ginger.museum.model.Exhibition;
import com.ginger.museum.utils.LogUtils;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebViewFragment;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabWidget;
import android.widget.TextView;
/**
 * Created by hungnguyendinhle on 04/15/15.
 */
public class MainActivity2 extends Activity implements OnTabChangeListener,
		TabContentFactory, OnNavigationListener, FragmentManager.OnBackStackChangedListener{


	final static String TAB_HOME_FRAGMENT = "HOME_FRAGMENT";
	final static String TAB_EXHIBITIONS_FRAGMENT = "EXHIBITIONS_FRAGMENT";
	final static String TAB_QUIZ_FRAGMENT = "QUIZ_FRAGMENT";
	final static String TAB_MAP_FRAGMENT = "MAP_FRAGMENT";
	final static String TAB_DETECT = "DETECT_FRAGMENT";
	final static String TAB_MUSEUM_FRAGMENT = "MUSEUM_FRAGMENT";
	final static String TAB_MYSDP_FRAGMENT = "MYSDP_FRAGMENT";
	final static String TAB_ARTWORK_FRAGMENT = "TAB_ARTWORK_FRAGMENT";
	final static String TAB_SEARCH = "SEARCH";
	final static String TAB_FAVORITE = "FAVORITE";
	private TabHost mTabHost;
	BaseFragment lastFragment = null;
	int containterId = android.R.id.tabcontent;
	LoadingFragment loadingDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        //hide action bar, set topbar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getActionBar().hide();
        getWindow().setBackgroundDrawableResource(R.drawable.background);
		//init tab
		initTab();

		//set home button
        findViewById(R.id.buttonHome).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showHomeScreen();
			}
		});
        //show Home
        showHomeScreen();
		//sync data
		syncData();
    }
	/*
	 *
	 */
	private void initTab() {
		//setup tab info
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setOnTabChangedListener(this);
		mTabHost.setup();
		//add tab
		mTabHost.addTab(
				mTabHost.newTabSpec(TAB_EXHIBITIONS_FRAGMENT).setIndicator(null, getResources().getDrawable(R.drawable.icon_exhibition)).setContent(this));
		mTabHost.addTab(
				mTabHost.newTabSpec(TAB_QUIZ_FRAGMENT).setIndicator(null, getResources().getDrawable(R.drawable.icon_quiz)).setContent(this));
		mTabHost.addTab(
				mTabHost.newTabSpec(TAB_DETECT).setIndicator(null, getResources().getDrawable(R.drawable._detecttabbaricon)).setContent(this));
		mTabHost.addTab(
				mTabHost.newTabSpec(TAB_MUSEUM_FRAGMENT).setIndicator(null, getResources().getDrawable(R.drawable._icon_fcac)).setContent(this));
		mTabHost.addTab(
				mTabHost.newTabSpec(TAB_MYSDP_FRAGMENT).setIndicator(null, getResources().getDrawable(R.drawable.icon_myspdp)).setContent(this));

		final TabWidget tabWidget = mTabHost.getTabWidget();
		String[] titles = new String[] {"EXHIBITIONS", "QUIZ", "DETECT", "MUSEUM", "MY SPDP"};
		String[] tabName = new String[] {TAB_EXHIBITIONS_FRAGMENT, TAB_QUIZ_FRAGMENT, TAB_DETECT, TAB_MUSEUM_FRAGMENT, TAB_MYSDP_FRAGMENT};
		for (int i = 0; i < tabWidget.getTabCount(); i++) {
            final View tab = tabWidget.getChildTabViewAt(i);
            tab.setTag(tabName[i]);
            //set tab style
            tab.setBackgroundColor(Color.TRANSPARENT);
            ((LinearLayout)tab).setOrientation(LinearLayout.VERTICAL);
            ((LinearLayout)tab).setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);

            //set title info
            final TextView title = (TextView) tab.findViewById(android.R.id.title);
            title.setText(titles[i]);
            LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
            params.leftMargin = 0;
            params.rightMargin = 0;
            params.topMargin = 0;
            params.bottomMargin = 0;
            tab.setPadding(0, 0, 0, 0);

            title.setSingleLine(true);
            title.setTextColor(Color.WHITE);
            title.setTextSize(9);
            title.setLayoutParams(params);
            title.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);

            //set icon
            ImageView icon = (ImageView) ((LinearLayout)tab).getChildAt(0);
            LayoutParams paramIcon = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
            icon.setLayoutParams(paramIcon);
            icon.setScaleType(ScaleType.CENTER);
            icon.setVisibility(View.VISIBLE);
			tab.setAlpha(0.6f);
            tab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Tab Listener", "Tab View On Clicked");
					unselectTab();
					v.setAlpha(1.0f);
                    onTabChanged(v.getTag().toString());
					//Color.rgb(51, 51, 51)
					tab.setBackgroundColor(Color.rgb(51, 51, 51));
                }
            });
        }
		getFragmentManager().addOnBackStackChangedListener(this);
	}
	/*

	 */
	public void unselectTab() {
		final TabWidget tabWidget = mTabHost.getTabWidget();
		for (int i = 0; i < tabWidget.getTabCount(); i++) {
			View tab = tabWidget.getChildTabViewAt(i);
			tab.setBackgroundColor(Color.rgb(64, 64, 64));
			tab.setAlpha(0.6f);
		}
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onTabChanged(String tabId) {
		//
		Log.d("onTabChanged", tabId);
		if (TAB_EXHIBITIONS_FRAGMENT.equalsIgnoreCase(tabId)) {
			showExhibitionsHome(-1);
		} else if (TAB_QUIZ_FRAGMENT.equalsIgnoreCase(tabId)){
			showFragment(QuizFragment.class, null, TAB_QUIZ_FRAGMENT);
		} else if (TAB_MYSDP_FRAGMENT.equalsIgnoreCase(tabId)) {
			showFragment(MySpdpFragment.class, null, TAB_MYSDP_FRAGMENT);
		}  else if (TAB_MUSEUM_FRAGMENT.equalsIgnoreCase(tabId)) {
			showFragment(AboutMuseumFragment.class, null, TAB_MUSEUM_FRAGMENT);
		} else if (TAB_DETECT.equalsIgnoreCase(tabId)) {
			showFragment(DetectFragment.class, null, TAB_DETECT);
		}
	}

	@Override
	public View createTabContent(String tag) {
		return findViewById(android.R.id.tabcontent);
	}
	/*
	 * 
	 */
	public BaseFragment showFragment(Class<?> fragmentClass, Bundle _args, String tag) {
		return showFragment(fragmentClass, _args, tag, false, false);
	}
	/*

	 */
	public BaseFragment showFragment(Class<?> fragmentClass, Bundle _args, String tag, boolean addBackToStack) {
		return showFragment(fragmentClass, _args, tag, addBackToStack, false);
	}
	/*
	 *
	 */
	public BaseFragment showFragment(Class<?> fragmentClass, Bundle _args, String tag, boolean addBackToStack, boolean isAnimation) {
		//TODO: should be remove all stack if addBackToStack is false ???
		if (!addBackToStack) {
			LogUtils.d("Entry Count:", "Entry Count:" + getFragmentManager().getBackStackEntryCount());
			int entryCount = getFragmentManager().getBackStackEntryCount();
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			while (entryCount > 0) {
				//pop stack
				String entryTag = getFragmentManager().getBackStackEntryAt(entryCount - 1).getName();
				Fragment f = getFragmentManager().findFragmentByTag(entryTag);
				if (f != null) {
					ft.detach(f);
				}
				getFragmentManager().popBackStackImmediate();
				entryCount--;
			}
			ft.commit();
		}
		//TODO: check code above: good or not
		BaseFragment findFragment = (BaseFragment)getFragmentManager().findFragmentById(containterId);
		if (findFragment != null) {
			lastFragment = findFragment;
		}
		if (lastFragment != null
				&& lastFragment.getClass().getName().equalsIgnoreCase(fragmentClass.getName())) {
			return lastFragment;
		}
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		if (lastFragment != null) {
			ft.detach(lastFragment);
		}
		if (isAnimation) {
			ft.setCustomAnimations(R.anim.slide_in_left, R.anim.fade);
		}
		BaseFragment f = (BaseFragment) getFragmentManager().findFragmentByTag(tag);
		if (f == null) {
			f = (BaseFragment) Fragment.instantiate(this, fragmentClass.getName());
			ft.add(containterId, f, tag);
			f.setOnNavigationListener(this);
		} else {
			ft.attach(f);
		}
		if (addBackToStack) {
			ft.addToBackStack(tag);
		}
		ft.commit();
		lastFragment = f;
		return lastFragment;
	}
	@Override
	public void showExhibitionDetail(int currentExhibitionIndex, Exhibition detail, int height) {
		LogUtils.d("showExhibitionDetail", "showExhibitionDetail: " + currentExhibitionIndex );
		BaseFragment fragment  = showFragment(ExhibitionDetailFragment.class, null, "showExhibitionDetail", false, true);
		((ExhibitionDetailFragment)fragment).setExhibitionItem(detail);
		((ExhibitionDetailFragment)fragment).setItemHeight(height);
		((ExhibitionDetailFragment)fragment).setCurrentExhibitionIndex(currentExhibitionIndex);
		((ExhibitionDetailFragment)fragment).setRefresh(true);
	}
	@Override
	public void showHomeScreen() {
		unselectTab();
		showFragment(HomeFragment.class, null, TAB_HOME_FRAGMENT);
	}

	@Override
	public void showDidYouKnow(DidYouKnow didYouKnow) {
		DidYouKnowFragment dialog = new DidYouKnowFragment();
		dialog.setDidYouKnow(didYouKnow);
		dialog.show(getFragmentManager(), "didyoukow");
	}

	@Override
	public void showArtworkDetail(Artwork artwork) {
		BaseFragment artworkFragment = showFragment(ArtworkMainFragment.class, null, TAB_ARTWORK_FRAGMENT, true);
		((ArtworkMainFragment)artworkFragment).setArtworkItem(artwork);
	}

	@Override
	public void showWebView(String link) {
		BaseFragment f = showFragment(WebBrowserFragment.class, null, "WEBVIEW", true);
		((WebBrowserFragment) f).setLink(link);
	}

	/*
	 * 
	 */
	@Override
	public void showExhibitionsHome(int currentItem) {
		Log.d("showExhibitionsHome", "currentItem: " + currentItem);
		int height = findViewById(android.R.id.tabcontent).getHeight();
		ExhibitionHomeFragment exhibitionFragment = (ExhibitionHomeFragment) showFragment(ExhibitionHomeFragment.class, null, TAB_EXHIBITIONS_FRAGMENT);
		exhibitionFragment.setCurrentItem(currentItem);
		exhibitionFragment.setItemHeight(height);
	}

	@Override
	public void showMapScreen() {
		showFragment(MapFragment.class, null, TAB_MAP_FRAGMENT);
	}

	@Override
	public void showSearchScreen() {
		showFragment(CatalogueFragment.class, null, TAB_SEARCH);
	}

	@Override
	public void onBackStack(BaseFragment fromFragment) {
	}
	@Override
	public void onBackStackChanged() {
		LogUtils.d("onBackStackChanged", "onBackStackChanged");

	}

	/*
         *
         */
	void syncData() {
		if (DataProvider.getInstance().isNeedToSync()) {
			loadingDialog = new LoadingFragment();
			loadingDialog.setNextProgess(100 / DataProvider.getInstance().getTotalTables());
			loadingDialog.show(getFragmentManager(), "loading");
			loadingDialog.setCancelable(false);
			DataProvider.getInstance().syncData(this, new DataProvider.SyncCallBack() {
				@Override
				public void done() {
					loadingDialog.dismiss();
					loadingDialog = null;
					if (lastFragment != null) {
						lastFragment.reloadData();
					}
				}

				@Override
				public void onProgress(int index, int total) {
					loadingDialog.setNextProgess((100 / DataProvider.getInstance().getTotalTables()) * (index + 1));
				}
			});
		}
	}

	@Override
	public void onBackPressed() {
		if (loadingDialog != null) {

		} else {
			super.onBackPressed();
		}
	}
}
