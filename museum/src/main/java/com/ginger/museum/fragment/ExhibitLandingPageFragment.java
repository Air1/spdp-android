package com.ginger.museum.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ginger.museum.PlayVideoActivity;
import com.ginger.museum.R;
import com.ginger.museum.model.Exhibition;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;

public class ExhibitLandingPageFragment extends BaseFragment {
	static final  int ANIMATION_TIME = 1000;
	View rootView; 
	ExhibitLandingPage1Listener landPagelistener;
	Button arrowDown;
	
	TextView textSponredBy;
	LinearLayout listSponsors;
	TextView textShortDescription;
	TextView textLongDescription;
	TextView title;
	ImageView textBorder;
	ImageView mainImage;
	LinearLayout layoutCuratorStatemen;
	LinearLayout layoutBrowserCatalog;
	boolean isShowLandingPage2 = false;
	boolean isShowingLandingPage2 = false;
	int shipUp = 300;
	int itemHeight = 0;
	Bitmap blueImage;

	public boolean isShowLandingPage2() {
		return isShowLandingPage2;
	}
	public void setLandPagelistener(ExhibitLandingPage1Listener landPagelistener) {
		this.landPagelistener = landPagelistener;
	}
	Exhibition exhibitionItem;

	public Exhibition getExhibitionItem() {
		return exhibitionItem;
	}

	public void setExhibitionItem(Exhibition exhibitionItem) {
		this.exhibitionItem = exhibitionItem;
	}

	public int getItemHeight() {
		return itemHeight;
	}

	public void setItemHeight(int itemHeight) {
		this.itemHeight = itemHeight;
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
	        rootView = inflater.inflate(R.layout.fragment_exhibition_landingpage, container, false);
	        rootView.findViewById(R.id.buttonClose).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (landPagelistener != null) {
						landPagelistener.onCloseExhibitionLandingPage1();
					}
				}
			});
	        arrowDown = (Button)rootView.findViewById(R.id.buttonMoreDetail);
	        arrowDown.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					showLandingPage2();
				}
			});
	        //load widget
	        textSponredBy = (TextView) rootView.findViewById(R.id.textSponredBy);
	        listSponsors = (LinearLayout) rootView.findViewById(R.id.listSponsors);
	        textShortDescription = (TextView) rootView.findViewById(R.id.textShortDescription);
			textLongDescription =  (TextView) rootView.findViewById(R.id.textLongDescription);
	        title = (TextView) rootView.findViewById(R.id.textTitle);
	        textBorder = (ImageView) rootView.findViewById(R.id.textBorder);
	        mainImage = (ImageView) rootView.findViewById(R.id.mainImage);
	        layoutCuratorStatemen = (LinearLayout) rootView.findViewById(R.id.layoutCuratorStatemen);
	        layoutBrowserCatalog = (LinearLayout) rootView.findViewById(R.id.layoutBrowserCatalog);
			layoutCuratorStatemen.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (exhibitionItem.getCurratorStatement() != null &&
							exhibitionItem.getCardDescription().length() > 0) {
						Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
						intent.putExtra(PlayVideoActivity.LINK, exhibitionItem.getCurratorStatement());
						startActivity(intent);
					}
				}
			});
			layoutBrowserCatalog.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (onNavigationListener != null) {
						onNavigationListener.showSearchScreen();
					}
				}
			});
		}
		shipUp = getResources().getDimensionPixelSize(R.dimen.s3_translation_top);
		showButtonArrow();
		isShowLandingPage2 = false;
        return rootView;
    }

	@Override
	public void onResume() {
		super.onResume();
		reloadData();
	}

	@Override
	public void reloadData() {
		super.reloadData();
		if (exhibitionItem != null) {
			//background
//			ImageUtils.displayImage(mContext, exhibitionItem.getCover().getUrl(), mainImage);
			ImageUtils.loadImageView(mContext,
					exhibitionItem.getCover().getUrl(), mainImage, new ImageUtils.LoadCallBack() {
						@Override
						public void onLoadCompleted(Bitmap image) {
							if (getActivity() == null) {
								return;
							}
							blueImage = image.copy(Bitmap.Config.ARGB_8888, false);
							//define this only once if blurring multiple times
							RenderScript rs = RenderScript.create(getActivity());
//								(...)
//this will blur the bitmapOriginal with a radius of 8 and save it in bitmapOriginal
							final Allocation input = Allocation.createFromBitmap(rs, blueImage); //use this constructor for best performance, because it uses USAGE_SHARED mode which reuses memory
							final Allocation output = Allocation.createTyped(rs, input.getType());
							final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, android.support.v8.renderscript.Element.U8_4(rs));
							script.setRadius(8f);
							script.setInput(input);
							script.forEach(output);
							output.copyTo(blueImage);
						}
					});

			title.setText(exhibitionItem.getName());
			textShortDescription.setText(exhibitionItem.getDescriptionShort());
			textLongDescription.setText(exhibitionItem.getDescriptionLong());

			if (exhibitionItem.getSponsors() != null) {
				LogUtils.d("listSponsors", "visible");
				listSponsors.setVisibility(View.VISIBLE);
				textSponredBy.setVisibility(View.VISIBLE);
				rootView.findViewById(R.id.sponsors1).setVisibility(View.VISIBLE);
				ImageUtils.displayImage(mContext, exhibitionItem.getSponsors().getUrl(), (ImageView) rootView.findViewById(R.id.sponsors1));

			} else {
				LogUtils.d("listSponsors", "invisible");
				rootView.findViewById(R.id.sponsors1).setVisibility(View.INVISIBLE);
				listSponsors.setVisibility(View.INVISIBLE);
				textSponredBy.setVisibility(View.INVISIBLE);
			}
		}
		rootView.findViewById(R.id.mainImage).getLayoutParams().height = itemHeight;
		if (isShowLandingPage2) {
			layoutBrowserCatalog.setVisibility(View.VISIBLE);
			layoutCuratorStatemen.setVisibility(View.VISIBLE);
			textLongDescription.setVisibility(View.VISIBLE);
			textShortDescription.setVisibility(View.INVISIBLE);
			listSponsors.setVisibility(View.INVISIBLE);
			textSponredBy.setVisibility(View.INVISIBLE);
			applyLandingPage2();
		}

	}

	/*
	 *
	 */
	public void showButtonArrow() {
		arrowDown.setVisibility(View.VISIBLE);
		arrowDown.setAlpha(1.0f);
		Animation fadeOut = new AlphaAnimation(1, 0);
	    fadeOut.setDuration(2000);
	    fadeOut.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				arrowDown.setVisibility(View.GONE);
			}
		});
	    arrowDown.startAnimation(fadeOut);
	}
	/*
	 * 
	 */
	public void showLandingPage2() {
		
		//face in animation
		if (isShowingLandingPage2) {
			return;
		}
		animationBackground();
		isShowingLandingPage2 = true;

	}
	/*
	 *
	 */
	public void applyLandingPage2() {
		mainImage.setAlpha(0.9f);
		int moveTop = -shipUp;
		title.setTranslationY(moveTop);
		textBorder.setTranslationY(moveTop);
		textShortDescription.setTranslationY(moveTop);
		textLongDescription.setTranslationY(moveTop);
		layoutCuratorStatemen.setTranslationY(moveTop);
		layoutBrowserCatalog.setTranslationY(moveTop);
		layoutCuratorStatemen.setTranslationX(0);
		layoutBrowserCatalog.setTranslationX(0);

	}
	void animationBackground() {
		//-	blur the background
		final Animation blueBackgroudAnimation = new AlphaAnimation(1, 0.9f);
		blueBackgroudAnimation.setDuration(500);
		blueBackgroudAnimation.setFillAfter(true);
		blueBackgroudAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				mainImage.setAlpha(1.0f);
				mainImage.setImageBitmap(blueImage);
				animationTitleToTop(shipUp);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
		mainImage.startAnimation(blueBackgroudAnimation);
	}

	void animationTitleToTop(final int shipUpBy) {
		//-	shift up the title (animation)
		Animation shipupTitle = new TranslateAnimation(0, 0, 0,  - shipUpBy);
		shipupTitle.setDuration(ANIMATION_TIME);
		shipupTitle.setFillAfter(true);
		title.startAnimation(shipupTitle);

		//shift up text boder
		Animation shipupBorder = new TranslateAnimation(0, 0, 0, 0 - shipUpBy);
		shipupBorder.setDuration(ANIMATION_TIME);
		shipupBorder.setFillAfter(true);
		textBorder.startAnimation(shipupBorder);

		//-	shift up the  short description
		Animation shipupDescription = new TranslateAnimation(0, 0, 0, 0 - shipUpBy);
		shipupDescription.setDuration(ANIMATION_TIME);
		shipupDescription.setFillAfter(true);
		shipupDescription.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				layoutCuratorStatemen.setTranslationY(-shipUpBy);
				layoutBrowserCatalog.setTranslationY(-shipUpBy);
				textShortDescription.setTranslationY(-shipUpBy);
				textLongDescription.setTranslationY(-shipUpBy);
				animationFadeoutShortDescription();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
		textShortDescription.startAnimation(shipupDescription);
		if (exhibitionItem.getSponsors() != null) {
			textSponredBy.startAnimation(shipupDescription);
			listSponsors.startAnimation(shipupDescription);
		}
	}

	void animationFadeoutShortDescription() {
		//fade out short description
		Animation fadeOutAnimation = new AlphaAnimation(1, 0.0f);
		fadeOutAnimation.setDuration(500);
		fadeOutAnimation.setFillAfter(true);
//		fadeOutAnimation.setFillBefore(true);
		fadeOutAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				animationFadeInDescription();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
		textShortDescription.startAnimation(fadeOutAnimation);
		//and sponrsed
		//-	sponsors disappear (fade out)
		if (exhibitionItem.getSponsors() != null) {
			Animation fadeOutTextSponreBy = new AlphaAnimation(1, 0);
			fadeOutTextSponreBy.setDuration(1500);
			fadeOutTextSponreBy.setFillAfter(true);
			textSponredBy.startAnimation(fadeOutTextSponreBy);
			Animation fadeOutListSponreBy = new AlphaAnimation(1, 0);
			fadeOutListSponreBy.setDuration(1500);
			fadeOutListSponreBy.setFillAfter(true);
			listSponsors.startAnimation(fadeOutListSponreBy);
		}
	}

	void animationFadeInDescription() {
		textShortDescription.setVisibility(View.INVISIBLE);
		textLongDescription.setVisibility(View.VISIBLE);
		Animation fadeInAnimation = new AlphaAnimation(0, 1.0f);
		fadeInAnimation.setDuration(ANIMATION_TIME);
		fadeInAnimation.setFillAfter(true);
//		fadeInAnimation.setFillBefore(true);
		fadeInAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				animationLink(shipUp);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
		textLongDescription.startAnimation(fadeInAnimation);
	}
	/*
	 * 
	 */
	void animationLink(int shipUpBy) {
		
		//what is your approach for multiple resolutions on android devices?
	    //“browse catalogue” (link to S5.0 - Catalogue) appears (text coming from the left)

  		Animation shipLeftAnimation = new TranslateAnimation(-layoutCuratorStatemen.getWidth()-30, 0, 0, 0);
  		shipLeftAnimation.setDuration(1000);
		shipLeftAnimation.setFillBefore(true);
  		shipLeftAnimation.setFillAfter(true);
		layoutCuratorStatemen.startAnimation(shipLeftAnimation);
  		
//  		Animation shipLeftAnimation2 = new TranslateAnimation(-layoutBrowserCatalog.getWidth()-30, 0, 0, 0);
//  		shipLeftAnimation2.setDuration(1000);
//		shipLeftAnimation2.setFillBefore(true);
//  		shipLeftAnimation2.setFillAfter(true);
  		layoutBrowserCatalog.startAnimation(shipLeftAnimation);
  		isShowLandingPage2 = true;
		shipLeftAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				isShowingLandingPage2 = false;
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
	}
	/*
	 * 
	 */
	public interface ExhibitLandingPage1Listener {
		void onCloseExhibitionLandingPage1();
	}
	
}
