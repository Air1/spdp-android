package com.ginger.museum.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ginger.museum.R;
import com.ginger.museum.utils.LogUtils;


public class BaseFragment extends Fragment {


	Context mContext;

	OnNavigationListener onNavigationListener;
	public OnNavigationListener getOnNavigationListener() {
		return onNavigationListener;
	}
	public void setOnNavigationListener(OnNavigationListener onNavigationListener) {
		this.onNavigationListener = onNavigationListener;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mContext = activity;
	}

	public void reloadData() {

	}
	public long getID() {
		return -1;
	}

	public void initTopBar(View rootView) {
		//set home button
		Button buttonHome = (Button) rootView.findViewById(R.id.buttonHome);
		if (buttonHome != null) {
			buttonHome.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (onNavigationListener != null) {
						onNavigationListener.showHomeScreen();
					}
				}
			});
		}
		//setmap button
		Button buttonMap = (Button) rootView.findViewById(R.id.buttonMap);
		if (buttonMap != null) {
			buttonMap.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (onNavigationListener != null) {
						onNavigationListener.showMapScreen();
					}
				}
			});
		}
		//setmap button
		Button buttonSearch = (Button) rootView.findViewById(R.id.buttonSearch);
		if (buttonSearch != null) {
			buttonSearch.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (onNavigationListener != null) {
						onNavigationListener.showSearchScreen();
					}
				}
			});
		}
		TextView screenTitle = (TextView)rootView.findViewById(R.id.screenTitle);
		if (screenTitle != null) {
			String title = getTitle();
			if (title != null && title.length() > 0) {
				screenTitle.setText(title);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		LogUtils.d(getClass().toString(), "OnResume");
	}

	/*
         *
         */
	public String getTitle() {
		return "";
	}
}
