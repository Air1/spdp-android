package com.ginger.museum.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ginger.museum.R;
import com.ginger.museum.model.DidYouKnow;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;

public class LoadingFragment extends DialogFragment {
    static final int DELAY_PROGRESS = 500;
    View rootView;
    Context mContext;
    ProgressBar progressBar;
    int nextProgess;
    Handler handleProgress = null;
    public LoadingFragment() {
        // Required empty public constructor
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.full_screen_dialog);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_loading, container, false);
            progressBar = (ProgressBar)rootView.findViewById(R.id.progressView);
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (handleProgress == null) {
            handleProgress = new Handler();
            handleProgress.postDelayed(progressThread, DELAY_PROGRESS);
        }
    }

    public void setNextProgess(int progress) {
        if (progressBar != null) {
            progressBar.setProgress(nextProgess);
        }
        this.nextProgess = progress;

    }

    Runnable progressThread = new Runnable() {
        @Override
        public void run() {
            if (progressBar.getProgress() < nextProgess) {
                progressBar.setProgress(progressBar.getProgress() + 1);
            }
            handleProgress.postDelayed(progressThread, DELAY_PROGRESS);
        }
    };

    @Override
    public void dismiss() {
        super.dismiss();
        handleProgress.removeCallbacks(progressThread);
    }



}
