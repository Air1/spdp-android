package com.ginger.museum.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ginger.museum.R;
import com.ginger.museum.adapter.BaseFragmentPagerAdapter;
import com.ginger.museum.fragment.HomeItem1Fragment.HomeItem1Listener;
import com.ginger.museum.model.Artwork;
import com.ginger.museum.model.DataProvider;
import com.ginger.museum.model.Home;
import com.ginger.museum.utils.LogUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;


public class ArtworkMainFragment extends BaseFragment implements ArtworkGalleryFragment.OnGalleryEvent {

	ViewPager pager;
	List<BaseFragment> cards;
	View rootView;
	Artwork artworkItem;

	public Artwork getArtworkItem() {
		return artworkItem;
	}

	public void setArtworkItem(Artwork artworkItem) {
		this.artworkItem = artworkItem;
	}

	public ArtworkMainFragment() {
		
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
	        rootView = inflater.inflate(R.layout.fragment_atrwork_main, container, false);
			pager = (ViewPager) rootView.findViewById(R.id.pagerArtwork);
		}
        return rootView;
    }

	@Override
	public void onResume() {
		super.onResume();
		reloadData();
	}

	@Override
	public void reloadData() {
		super.reloadData();
		//The first card is always a cards is hardcoded data
		if (artworkItem != null) {
			if (cards == null || cards.size() == 0) {
				LogUtils.d(getClass().toString(), "show artwork detail and slideshow");
				cards = new ArrayList<BaseFragment>();
				ArtworkDetailFragment detailFragment = new ArtworkDetailFragment();
				detailFragment.setArtworkItem(artworkItem);
				detailFragment.setOnNavigationListener(onNavigationListener);
				cards.add(detailFragment);
				if (artworkItem.getSlideshow() != null) {
					ArtworkSlideShowFragment galleryFragment = new ArtworkSlideShowFragment();
					galleryFragment.setArtworkItem(artworkItem);
					galleryFragment.setOnGalleryEvent(ArtworkMainFragment.this);
					cards.add(galleryFragment);
				}
				BaseFragmentPagerAdapter adapter = new BaseFragmentPagerAdapter(getFragmentManager(), cards);
				pager.setAdapter(adapter);
			} else {
				((ArtworkDetailFragment)cards.get(0)).setArtworkItem(artworkItem);
				cards.get(0).reloadData();
				if (cards.size() > 1) {
					((ArtworkSlideShowFragment) cards.get(1)).setArtworkItem(artworkItem);
					cards.get(1).reloadData();
				}
			}
		}
	}

	@Override
	public void onItemArtworkClicked(Artwork artwork) {
		ArtworkDetailFragment detailFragment = (ArtworkDetailFragment)cards.get(0);
		detailFragment.setArtworkItem(artwork);
		detailFragment.reloadData();
		pager.setCurrentItem(0, true);
	}

	@Override
	public void onBack() {
		pager.setCurrentItem(0, true);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		//should be remove all child fragments
		if (cards.size() > 0) {
			LogUtils.d("onDestroy", "should be remove all child fragments");
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			for (Fragment f : cards) {
				ft.detach(f);
			}
			ft.commit();
		}
	}
}
