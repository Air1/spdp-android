package com.ginger.museum.fragment;

import android.app.FragmentTransaction;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ginger.museum.R;
import com.ginger.museum.adapter.BaseFragmentPagerAdapter;
import com.ginger.museum.fragment.HomeItem1Fragment.HomeItem1Listener;
import com.ginger.museum.model.DataProvider;
import com.ginger.museum.model.Home;
import com.ginger.museum.model.Museum;
import com.ginger.museum.utils.LogUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hungnguyendinhle on 04/21/15.
 */
public class AboutMuseumFragment extends BaseFragment {

	ViewPager pageMuseumCard;
	List<BaseFragment> cards;
	View rootView;
	BaseFragmentPagerAdapter pagerAdapter;
	public AboutMuseumFragment() {
		
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
	        rootView = inflater.inflate(R.layout.fragment_about_museum, container, false);
	        pageMuseumCard = (ViewPager) rootView.findViewById(R.id.pagerMuseumCard);
			//init first card
			cards = new ArrayList<BaseFragment>();
			pagerAdapter = new BaseFragmentPagerAdapter(getFragmentManager(), cards);
			pageMuseumCard.setAdapter(pagerAdapter);
	        //set indicator
	        CirclePageIndicator indicator = (CirclePageIndicator) rootView.findViewById(R.id.indicator);
	        indicator.setViewPager(pageMuseumCard);
	        indicator.setPageColor(getResources().getColor(R.color.pager_color));
	        indicator.setFillColor(getResources().getColor(R.color.pager_color_fill));
	        indicator.setStrokeColor(getResources().getColor(R.color.pager_color_fill));
	        indicator.setStrokeWidth(0);
	        indicator.setRadius(BitmapFactory.decodeResource(getResources(), R.drawable._dot_active).getWidth()/2);
			reloadData();
		}
        return rootView;
    }

	@Override
	public void onResume() {
		super.onResume();


	}

	@Override
	public void reloadData() {
		super.reloadData();
		//The first card is always a cards is hardcoded data

		//get data from table Home
		DataProvider.getInstance().fetchMuseumRecords(new FindCallback<Museum>() {
			@Override
			public void done(List<Museum> list, ParseException e) {
				//clear all except first card
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				for (int i = cards.size() - 1; i >= 0; i--) {
					ft.detach(cards.get(i));
					cards.remove(i);
				}
				ft.commit();
				for (int i = 0; i < list.size(); i++) {
					LogUtils.d(AboutMuseumFragment.class.toString(), "Total Home Card: " + i);
					ItemMuseumFragment card2 = new ItemMuseumFragment();
					card2.setMuseumItem(list.get(i));
					cards.add(card2);
				}
				pagerAdapter.notifyDataSetChanged();
			}
		});
	}
}
