package com.ginger.museum.fragment;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Bitmap;
import com.ginger.museum.R;
import com.ginger.museum.model.Exhibition;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;
import com.ginger.museum.utils.UiUtils;
import com.ginger.museup.ui.OnSwipeTouchListener;


public class ExhibitLandingSection1Fragment extends BaseFragment {
	private static final int ANIMATION_TIME = 1000;
	View rootView;
	ImageView section1Artwork;
	ImageView imagePinPlus;
	boolean isShowingSectionArtwork = false;
	TextView section1Description;
	TextView section1Title;
	Exhibition exhibitionItem;
	int artworkImageMargin = 0;
	public Exhibition getExhibitionItem() {
		return exhibitionItem;
	}
	int screenWidth;
	int imageSizePinPlus;
	public void setExhibitionItem(Exhibition exhibitionItem) {
		this.exhibitionItem = exhibitionItem;
	}
	boolean isShowAnimation = false;

	public boolean isShowAnimation() {
		return isShowAnimation;
	}

	public void setIsShowAnimation(boolean isShowAnimation) {
		this.isShowAnimation = isShowAnimation;
	}
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
	        rootView = inflater.inflate(R.layout.fragment_exhibition_section1, container, false);
	        section1Artwork = (ImageView)rootView.findViewById(R.id.section1Artwork);
			imagePinPlus =  (ImageView)rootView.findViewById(R.id.imagePinPlus);
	        section1Artwork.setOnClickListener(onClickEvent);
			imagePinPlus.setOnClickListener(onClickEvent);
			//Todo: good or not
			RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams)section1Artwork.getLayoutParams();
			param.width = UiUtils.getScreenWidth(mContext);
			artworkImageMargin = mContext.getResources().getDimensionPixelSize(R.dimen.s3_section1_margin_left) +  mContext.getResources().getDimensionPixelSize(R.dimen.s3_section_description_width)  + mContext.getResources().getDimensionPixelSize(R.dimen.s3_section3_margin_image_text);
			section1Artwork.setLayoutParams(param);
			section1Artwork.setTranslationX(artworkImageMargin);
			imagePinPlus.setTranslationX(artworkImageMargin);
			section1Artwork.setOnTouchListener(new OnSwipeTouchListener(mContext) {
				@Override
				public void onSwipeLeft() {

				}

				@Override
				public void onSwipeRight() {
					hideArtwork();
				}

				@Override
				public void onTap() {
					if (!isShowingSectionArtwork) {
						showArtwork();
					} else {
						LogUtils.d("Show Artworks:", "getSection1Artwork");
						if (onNavigationListener != null) {
							onNavigationListener.showArtworkDetail(exhibitionItem.getSection1Artwork());
						}
					}
				}
			});
			section1Title = (TextView)rootView.findViewById(R.id.section1Title);
	        isShowingSectionArtwork = false;
	        section1Description = (TextView) rootView.findViewById(R.id.section1Description);
			section1Title.setText("");
			section1Description.setText("");
			rootView.findViewById(R.id.buttonDidYouKnow).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (onNavigationListener != null) {
						onNavigationListener.showDidYouKnow(exhibitionItem.getSection1DidYouKnow());
					}
				}
			});
			screenWidth = UiUtils.getScreenWidth(mContext);
			imageSizePinPlus = getResources().getDimensionPixelSize(R.dimen.image_size_pinplus);
		}

		return rootView;
    }

	View.OnClickListener onClickEvent = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			if (!isShowingSectionArtwork) {
				showArtwork();
			} else {
				if (onNavigationListener != null) {
					onNavigationListener.showArtworkDetail(exhibitionItem.getSection1Artwork());
				}
			}
		}
	};


	@Override
	public void onResume() {
		super.onResume();
		reloadData();

	}

	@Override
	public void reloadData() {
		super.reloadData();
		if (exhibitionItem != null) {
			section1Title.setText(exhibitionItem.getSection1Title());
			section1Description.setText(exhibitionItem.getSection1Description());
			LogUtils.d(getClass().toString(), "section1Description: " + exhibitionItem.getSection1Description());
			if (exhibitionItem.getSection1Artwork() != null && exhibitionItem.getSection1Artwork().getImage() != null) {
				LogUtils.d("Url", exhibitionItem.getSection1Artwork().getImage().getUrl());
				ImageUtils.loadImageView(mContext,
						exhibitionItem.getSection1Artwork().getImage().getUrl(),
						section1Artwork, new ImageUtils.LoadCallBack() {
							@Override
							public void onLoadCompleted(Bitmap image) {
								//set layout params
								if (getActivity() == null) {
									return;
								}
								LogUtils.d("Image size section artwork 1:", image.getWidth() + "/" + image.getHeight());
								RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams)section1Artwork.getLayoutParams();

								param.width = screenWidth;
								if (image != null) {
									if (param.width > image.getWidth()) {
										param.width = image.getWidth();
										param.height = image.getHeight();
									} else {
										float scaleX = screenWidth / 1080.0f;
										param.width = (int)(image.getWidth() * scaleX);
										param.height = (int)(image.getHeight() * scaleX);
										if (param.width > screenWidth) {
											param.width = screenWidth - imageSizePinPlus;
										}
									}
									param.height = (int)(param.width * (image.getHeight()*1.0f/image.getWidth()));
								}
								artworkImageMargin = mContext.getResources().getDimensionPixelSize(R.dimen.s3_section1_margin_left) + mContext.getResources().getDimensionPixelSize(R.dimen.s3_section_description_width) + mContext.getResources().getDimensionPixelSize(R.dimen.s3_section3_margin_image_text);
								section1Artwork.setLayoutParams(param);
								if (!isShowAnimation) {
									section1Artwork.setTranslationX(artworkImageMargin + 500);
									imagePinPlus.setTranslationX(artworkImageMargin + 500);
								} else {
									section1Artwork.setTranslationX(artworkImageMargin);
									imagePinPlus.setTranslationX(artworkImageMargin);
								}
								imagePinPlus.invalidate();
								rootView.findViewById(R.id.layoutArtwork).invalidate();
							}
						});
			}
			if (!isShowAnimation) {
				section1Description.setAlpha(0.0f);
			}
			if (exhibitionItem.getSection1DidYouKnow() != null) {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.VISIBLE);
			} else {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.INVISIBLE);
			}
		}
	}
	/*
	 *
	 */
	public void showAnimation() {
		//face in animation
		ObjectAnimator.ofFloat(section1Description, "alpha", 0.0f, 1.0f).setDuration(1000).start();

		//animation image
		ObjectAnimator mover = ObjectAnimator.ofFloat(section1Artwork,
				"translationX", artworkImageMargin + 500, artworkImageMargin);
		mover.setDuration(1000);
		mover.start();

		ObjectAnimator.ofFloat(imagePinPlus,
				"translationX", artworkImageMargin + 500, artworkImageMargin).setDuration(1000).start();

	}
	int translateImageX;
	/*
	 * 
	 */
	public void showArtwork() {
		isShowingSectionArtwork = true;
		int screenWidth = UiUtils.getScreenWidth(getActivity());
		translateImageX = screenWidth/2 - section1Artwork.getWidth()/2 - section1Artwork.getLeft();
		ObjectAnimator mover = ObjectAnimator.ofFloat(section1Artwork,
				"translationX", artworkImageMargin, translateImageX);
		mover.setDuration(1000);
		mover.start();

		ObjectAnimator.ofFloat(imagePinPlus,
				"translationX", artworkImageMargin, translateImageX).setDuration(1000).start();
	}
	/*
	 *
	 */
	void hideArtwork() {
		isShowingSectionArtwork = false;
		ObjectAnimator mover = ObjectAnimator.ofFloat(section1Artwork,
				"translationX", translateImageX, artworkImageMargin);
		mover.setDuration(1000);
		mover.start();

		ObjectAnimator.ofFloat(imagePinPlus,
				"translationX", translateImageX, artworkImageMargin).setDuration(1000).start();
	}
	
}
