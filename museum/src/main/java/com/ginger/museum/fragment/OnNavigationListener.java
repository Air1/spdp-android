package com.ginger.museum.fragment;

import com.ginger.museum.model.Artwork;
import com.ginger.museum.model.DidYouKnow;
import com.ginger.museum.model.Exhibition;
import com.parse.ParseObject;
/**
 * Created by hungnguyendinhle on 04/22/15.
 */
public interface OnNavigationListener {

	void showExhibitionsHome(int currentItem);
	void showSearchScreen();
	void showMapScreen();
	void showHomeScreen();
	void showDidYouKnow(DidYouKnow didYouKnow);
	void showArtworkDetail(Artwork artwork);
	void showWebView(String link);
	void onBackStack(BaseFragment fromFragment);
	void showExhibitionDetail(int currentExhibitionIndex, Exhibition detail, int height);
}
