package com.ginger.museum.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.etsy.android.grid.StaggeredGridView;
import com.ginger.museum.R;
import com.ginger.museum.adapter.ArtworksAdapter;
import com.ginger.museum.model.Artwork;
import com.ginger.museum.model.DataProvider;
import com.ginger.museum.model.Exhibition;
import com.ginger.museum.utils.LogUtils;
import com.ginger.museup.ui.CustomTextView;
import com.parse.FindCallback;
import com.parse.ParseException;
import java.util.ArrayList;
import java.util.List;


public class CatalogueFragment extends BaseFragment {

    ArtworksAdapter adapter = null;
    List<Artwork> listArtworks;
    View rootView;
    EditText textSearch;
    RelativeLayout layoutExhibition;
    TextView titleResultView;
    StaggeredGridView gridView;
    public CatalogueFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_catalogue, container, false);

            gridView = (StaggeredGridView) rootView.findViewById(R.id.listResult);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    if (onNavigationListener != null) {
                        LogUtils.d("Index", "Index :" + i);
                        onNavigationListener.showArtworkDetail(listArtworks.get(i-1));
                    }
                }
            });
            textSearch = (EditText)rootView.findViewById(R.id.textSearch);

            rootView.findViewById(R.id.buttonSearchArtwork).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    search();
                }
            });
            textSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
//                    search();
                    return false;
                }
            });
            textSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    search();
                }
            });
            layoutExhibition = (RelativeLayout)rootView.findViewById(R.id.layoutExhibition);
            layoutExhibition.removeAllViews();

            titleResultView = (TextView)rootView.findViewById(R.id.textTitleResult);
            initTopBar(rootView);
            LinearLayout headerView = (LinearLayout)rootView.findViewById(R.id.headerView);
            RelativeLayout parent = (RelativeLayout)headerView.getParent();
            parent.removeView(headerView);
            gridView.addHeaderView(headerView);
            listArtworks = new ArrayList<Artwork>();
            adapter = new ArtworksAdapter(mContext, listArtworks);
            gridView.setAdapter(adapter);
            DataProvider.getInstance().fetchExhibitionRecords(new FindCallback<Exhibition>() {
                @Override
                public void done(List<Exhibition> list, ParseException e) {
                    if (list != null) {
                        buildExhibitionType(list);
                        search();
                    }
                }
            });
        }
        return rootView;
    }
    @Override
    public String getTitle() {
        return getString(R.string.screen_title_catalog);
    }
    /*
     *
     */
    View.OnClickListener onExhibitionClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TextView selectedExhibition = (TextView)view;
            if (selectedExhibition.isSelected()) {
                selectedExhibition.setBackgroundResource(R.drawable.button1_off);
            } else {
                selectedExhibition.setBackgroundResource(R.drawable.button1_on);
            }
            selectedExhibition.setSelected(!selectedExhibition.isSelected());
            search();
        }
    };

    /*
     *
     */
    void search() {
        String text = textSearch.getText().toString();
        final ArrayList<String> listExhibitons = new ArrayList<String>();
        for (int i = 0; i < layoutExhibition.getChildCount(); i++) {
            TextView button = (TextView)layoutExhibition.getChildAt(i);
            if (button.isSelected()) {
                listExhibitons.add(((Exhibition)button.getTag()).getObjectId());
            }
        }
        DataProvider.getInstance().searchArtwork(text, listExhibitons, new FindCallback<Artwork>() {
            @Override
            public void done(List<Artwork> list, ParseException e) {
                listArtworks.clear();
                if (list != null) {
                    listArtworks.addAll(list);
                }
                if (list.size() > 1) {
                    titleResultView.setText(listArtworks.size() + " results");
                } else {
                    titleResultView.setText(listArtworks.size() + " result");
                }
                adapter.notifyDataSetChanged();
//                adapter = new ArtworksAdapter(mContext, listArtworks);
//                gridView.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onNavigationListener = (OnNavigationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
    /*
     *
     */
    public void buildExhibitionType(List<Exhibition> list) {
        int prevId = -1;
        for (int i = 0; i < list.size(); i++) {
            Exhibition exhibition = list.get(i);
            CustomTextView exButton = new CustomTextView(mContext);
            exButton.setText(exhibition.getName());
            exButton.setTag(exhibition);
            exButton.setBackgroundResource(R.drawable.button1_on);
            exButton.setTextSize(13);
            exButton.setTextColor(Color.WHITE);
            exButton.setPadding(10, 5, 10, 5);
            exButton.setGravity(Gravity.CENTER);
            RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            param.leftMargin = param.topMargin = param.rightMargin = param.bottomMargin = getResources().getDimensionPixelSize(R.dimen.list_exhibition_type_margin);
            exButton.setId(10001 + i);
            if (prevId != -1) {
                if (i % 2 == 1) {
                    param.addRule(RelativeLayout.RIGHT_OF, prevId);
                    if (i > 1) {
                        param.addRule(RelativeLayout.BELOW, prevId-2);
                    }
                } else {
                    param.addRule(RelativeLayout.BELOW, prevId);
                }
            }
            prevId = 10001 + i;
            exButton.setLayoutParams(param);
            exButton.setOnClickListener(onExhibitionClickEvent);
            exButton.setSelected(true);
            layoutExhibition.addView(exButton);
        }

    }

}
