package com.ginger.museum.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ginger.museum.R;
import com.ginger.museum.model.Artwork;
import com.ginger.museum.model.DataProvider;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museup.ui.FlipImageView;
import com.parse.GetCallback;
import com.parse.ParseException;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetectFragment extends BaseFragment {

    View rootView;
    FlipImageView cubeView;
    ImageView handView;
    ImageView searchFrame;
    View layoutDetect;

    public DetectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_detect_artwork, container, false);
            cubeView = (FlipImageView) rootView.findViewById(R.id.cube);
            handView = (ImageView) rootView.findViewById(R.id.hand);
            layoutDetect = rootView.findViewById(R.id.layoutDetect);
            searchFrame = (ImageView) rootView.findViewById(R.id.searchFrame);
            initTopBar(rootView);
            startDetecting();
        }
        return rootView;
    }

    @Override
    public String getTitle() {
        return getResources().getString(R.string.screen_title_detect);
    }

    /*
     * call this function when starting detecting artwork
     */
    public void startDetecting() {
        //show search animation
        handView.setVisibility(View.VISIBLE);
        cubeView.setVisibility(View.VISIBLE);
        searchFrame.setVisibility(View.VISIBLE);

        //hide result
        layoutDetect.setVisibility(View.INVISIBLE);
        rootView.findViewById(R.id.cubeHand).setVisibility(View.INVISIBLE);
        //
        //TODO: should be remove 2 line codes below
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                detectArtwork("7VwUWjRORA");
            }
        }, 6000);

    }
    /*
     * call this functin when detect an artwork by beatcon
     */
    public void detectArtwork(String artworkID) {
        //hide search
        handView.setVisibility(View.INVISIBLE);
        cubeView.setVisibility(View.INVISIBLE);
        cubeView.clearAnimation();
        cubeView.setAnimated(false);
        cubeView.getFlipAnimation().cancel();
        searchFrame.setVisibility(View.INVISIBLE);

        //show result
        layoutDetect.setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.cubeHand).setVisibility(View.VISIBLE);
        DataProvider.getInstance().getArtwork(artworkID, new GetCallback<Artwork>() {
            @Override
            public void done(Artwork artwork, ParseException e) {
                artwork.parse();
                showArtwork(artwork);
            }
        });

    }
    /*
     *
     */
    public void showArtwork(final Artwork artwork) {
        if (artwork != null) {
            ImageView imageArtwork = (ImageView) rootView.findViewById(R.id.imageArtwork);
            if (imageArtwork != null) {
                ImageUtils.displayImage(mContext, artwork.getImage().getUrl(), imageArtwork);
                imageArtwork.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onNavigationListener != null) {
                            onNavigationListener.showArtworkDetail(artwork);
                        }
                    }
                });
            }
            ((TextView)rootView.findViewById(R.id.textTitle)).setText(artwork.getTitle());
            ((TextView)rootView.findViewById(R.id.textSubTitle)).setText(artwork.getDate());
        }
    }

}
