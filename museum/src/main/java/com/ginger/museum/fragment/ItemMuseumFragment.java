package com.ginger.museum.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ginger.museum.R;
import com.ginger.museum.model.Home;
import com.ginger.museum.model.Museum;
import com.ginger.museum.utils.ImageUtils;
/**
 * Created by hungnguyendinhle on 04/21/15.
 */
public class ItemMuseumFragment extends BaseFragment {

	Museum museumItem;
	TextView textTitle;
	TextView textSubTitle;
	TextView textDescription;
	ImageView imageMuseum;
	View rootView = null;

	public Museum getMuseumItem() {
		return museumItem;
	}

	public void setMuseumItem(Museum museumItem) {
		this.museumItem = museumItem;
	}

	public ItemMuseumFragment() {
		
	}
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_item_museum, container, false);
			textTitle = (TextView) rootView.findViewById(R.id.textTitle);
			textSubTitle = (TextView) rootView.findViewById(R.id.textSubTitle);
			textDescription = (TextView) rootView.findViewById(R.id.textDescription);
			imageMuseum = (ImageView)rootView.findViewById(R.id.imageMuseum);
			rootView.findViewById(R.id.buttonMoreInfo).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					viewMoreInfo();
				}
			});
			rootView.findViewById(R.id.buttonDidYouKnow).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (onNavigationListener != null) {
						onNavigationListener.showDidYouKnow(museumItem.getDidYouKnow());
					}
				}
			});
		}
        return rootView;
    }

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			onNavigationListener = (OnNavigationListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void reloadData() {
		super.reloadData();
		if (museumItem != null) {
			textTitle.setText(museumItem.getTitle());
			textSubTitle.setText(museumItem.getSubTitle());
			textDescription.setText(museumItem.getDescription());
			ImageUtils.displayImage(getActivity(), museumItem.getImage().getUrl(), imageMuseum);
			if (museumItem.getLink() != null && museumItem.getLink().length() > 0) {
				rootView.findViewById(R.id.buttonMoreInfo).setVisibility(View.VISIBLE);
			} else {
				rootView.findViewById(R.id.buttonMoreInfo).setVisibility(View.GONE);
			}
			if (museumItem.getDidYouKnow() != null) {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.VISIBLE);
			} else {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.GONE);
			}
		}
	}
	/*
	 *
	 */
	void viewMoreInfo() {
		if (onNavigationListener != null) {
			onNavigationListener.showWebView(museumItem.getLink());
		}
	}
	@Override
	public void onResume() {
		super.onResume();
		reloadData();
	}

}
