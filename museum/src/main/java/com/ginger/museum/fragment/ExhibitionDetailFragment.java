package com.ginger.museum.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.ginger.museum.R;
import com.ginger.museum.model.Exhibition;
import com.ginger.museum.utils.LogUtils;
import com.ginger.museup.ui.CustomScrollView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExhibitionDetailFragment extends BaseFragment implements ExhibitionItemFragment.ExhibitionNavigation
        , ExhibitLandingPageFragment.ExhibitLandingPage1Listener {

    View rootView;
    int itemHeight;
    Exhibition exhibitionItem;
    ExhibitLandingPageFragment exhibitLandingPageFragment;
    FrameLayout section1Layout;
    ExhibitLandingSection1Fragment section1Fragment;
    FrameLayout section3Layout;
    ExhibitLandingSection3Fragment section3Fragment;
    CustomScrollView mainScrollView;
    int exhibitionIndex;
    boolean refresh;


    public ExhibitionDetailFragment() {
        // Required empty public constructor
    }

    public Exhibition getExhibitionItem() {
        return exhibitionItem;
    }

    public void setExhibitionItem(Exhibition exhibitionItem) {
        this.exhibitionItem = exhibitionItem;
    }

    public boolean isRefresh() {
        return refresh;
    }

    public void setRefresh(boolean refresh) {
        this.refresh = refresh;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_exhibition_detail, container, false);
            section1Layout = (FrameLayout)rootView.findViewById(R.id.exhibitionSection1);
            section3Layout = (FrameLayout)rootView.findViewById(R.id.exhibitionSection3);
            mainScrollView = (CustomScrollView)rootView.findViewById(R.id.mainScrollView);

            mainScrollView.setOnScrollViewListener(new CustomScrollView.OnScrollViewListener() {
                @Override
                public void onScrollChanged(CustomScrollView v, int l, int t, int oldl,
                                            int oldt) {
                    if (mainScrollView.getScrollY() == 0) {
                        if (!exhibitLandingPageFragment.isShowLandingPage2()) {
                            mainScrollView.setEnabled(false);
                        }
                    }
                    if (mainScrollView.getScrollY() >= section1Layout.getTop() - section1Layout.getHeight()/2) {
                        if (!section1Fragment.isShowAnimation) {
                            section1Fragment.showAnimation();
                            section1Fragment.setIsShowAnimation(true);
                        }
                    }
                    if (mainScrollView.getScrollY() >= section3Layout.getTop() - section3Layout.getHeight()/2) {
                        if (!section3Fragment.isShowAnimation) {
                            section3Fragment.showAnimation();
                            section3Fragment.setIsShowAnimation(true);
                        }
                    }
                }

                @Override
                public void onScrollTop() {

                }

                @Override
                public void onScrollDown() {
                    Log.d("onScrollDown", "On ScrollDown");
                    if (!mainScrollView.isEnabled()) {
                        exhibitLandingPageFragment.showLandingPage2();
                        mainScrollView.setEnabled(true);
                    }
                }
            });
            rootView.findViewById(R.id.buttonClose).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LogUtils.d("Exhibition INdex:", "" + exhibitionIndex);
                    onNavigationListener.showExhibitionsHome(exhibitionIndex);
                }
            });
        }
        return rootView;
    }
    public void setCurrentExhibitionIndex(int currentExhibitionIndex) {
        exhibitionIndex = currentExhibitionIndex;
    }
    @Override
    public void onResume() {
        super.onResume();
        //set height
        LogUtils.d("Exhibition Detail Fragment", "onResume data");
        rootView.findViewById(R.id.mainScrollView).getLayoutParams().height = itemHeight;
//        rootView.findViewById(R.id.exhibitionDetail).getLayoutParams().height = itemHeight;
        rootView.findViewById(R.id.exhibitionDetail).setMinimumHeight(itemHeight);
        rootView.findViewById(R.id.exhibitionLandingQuote).getLayoutParams().height = itemHeight;
        if (exhibitionItem != null) {
            showExhibitionDetail(exhibitionItem);
            if (refresh) {
                mainScrollView.scrollTo(0, 0);
                mainScrollView.setEnabled(false);
            } else {
                LogUtils.d("Exhibition Detail Fragment", "show animation");
                section1Fragment.setIsShowAnimation(true);
                section3Fragment.setIsShowAnimation(true);
            }
            if (mainScrollView.isEnabled()) {
//                exhibitLandingPageFragment.setShowLandingPage(true);
            }
            refresh = false;
        }
    }

    /*
         *
         */
    public void setItemHeight(int height) {
        itemHeight = height;
    }
    public void showExhibitionDetail(Exhibition item ) {
        setExhibitLandingPageFragment(item);
        setExhibitLandingSection1Fragment(item);
        setExhibitLandingSection2Fragment(item);
        setExhibitLandingSection3Fragment(item);
        setExhibitLandingQuoteFragment(item);
    }
    /*

     */
    public void setExhibitLandingPageFragment(Exhibition exhibition) {
        exhibitLandingPageFragment = new ExhibitLandingPageFragment();
        exhibitLandingPageFragment.setExhibitionItem(exhibition);
        exhibitLandingPageFragment.setItemHeight(itemHeight);
        exhibitLandingPageFragment.setOnNavigationListener(onNavigationListener);
        exhibitLandingPageFragment.setLandPagelistener(this);
        getFragmentManager().beginTransaction()
                .replace(R.id.exhibitionDetail, exhibitLandingPageFragment)
                .commit();
    }
    public void setExhibitLandingSection1Fragment(Exhibition exhibition) {

        section1Fragment = new ExhibitLandingSection1Fragment();
        section1Fragment.setExhibitionItem(exhibition);
        section1Fragment.setOnNavigationListener(onNavigationListener);
        getFragmentManager().beginTransaction()
                .replace(R.id.exhibitionSection1, section1Fragment)
                .commit();
    }
    public void setExhibitLandingSection2Fragment(Exhibition exhibition) {
        ExhibitLandingSection2Fragment section2 = new ExhibitLandingSection2Fragment();
        section2.setExhibitionItem(exhibition);
        section2.setOnNavigationListener(onNavigationListener);
        getFragmentManager().beginTransaction()
                .replace(R.id.exhibitionSection2, section2)
                .commit();
    }
    public void setExhibitLandingSection3Fragment(Exhibition exhibition) {
        section3Fragment = new ExhibitLandingSection3Fragment();
        section3Fragment.setExhibitionItem(exhibition);
        section3Fragment.setOnNavigationListener(onNavigationListener);
        getFragmentManager().beginTransaction()
                .replace(R.id.exhibitionSection3, section3Fragment)
                .commit();
    }
    public void setExhibitLandingQuoteFragment(Exhibition exhibition) {
        ExhibitLandingQuoteFragment quote = new ExhibitLandingQuoteFragment();
        quote.setExhibitionItem(exhibition);
        quote.setOnNavigationListener(onNavigationListener);
        getFragmentManager().beginTransaction()
                .replace(R.id.exhibitionLandingQuote, quote)
                .commit();
    }
    @Override
    public void onCloseExhibitionLandingPage1() {

    }

    @Override
    public void onNavigateToExhibitionLandingPage1(Exhibition exhibition) {

    }
}
