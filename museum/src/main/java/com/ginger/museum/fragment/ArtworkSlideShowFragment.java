package com.ginger.museum.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ginger.museum.R;
import com.ginger.museum.adapter.GalleryAdapter;
import com.ginger.museum.adapter.SlideshowAdapter;
import com.ginger.museum.model.Artwork;
import com.ginger.museum.model.DataProvider;
import com.ginger.museum.model.Exhibition;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;


public class ArtworkSlideShowFragment extends BaseFragment {

    SlideshowAdapter adapter = null;
    List<Map<String, Object>> listArtworks;
    View rootView;
    Artwork artworkItem;
    ArtworkGalleryFragment.OnGalleryEvent onGalleryEvent;
    public ArtworkSlideShowFragment() {
        // Required empty public constructor
    }

    public Artwork getArtworkItem() {
        return artworkItem;
    }

    public void setArtworkItem(Artwork artworkItem) {
        this.artworkItem = artworkItem;
    }

    public void setOnGalleryEvent(ArtworkGalleryFragment.OnGalleryEvent onGalleryEvent) {
        this.onGalleryEvent = onGalleryEvent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_artwork_galerry, container, false);
            ListView listView = (ListView) rootView.findViewById(R.id.listArtworks);
            listArtworks = new ArrayList<Map<String, Object>>();
            adapter = new SlideshowAdapter(mContext, listArtworks);
            listView.setAdapter(adapter);
            rootView.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onGalleryEvent != null) {
                        onGalleryEvent.onBack();
                    }
                }
            });
        }
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onNavigationListener = (OnNavigationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadData();
    }

    @Override
    public void reloadData() {
        super.reloadData();
        if (artworkItem != null) {
            if (artworkItem.getSlideshow() != null) {
                listArtworks.clear();
                listArtworks.addAll(artworkItem.getSlideshow());
                adapter.notifyDataSetChanged();
            }
        }
    }
    @Override
    public long getID() {
        return Calendar.getInstance().getTimeInMillis();
    }
}
