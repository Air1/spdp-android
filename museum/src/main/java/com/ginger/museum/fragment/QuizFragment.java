package com.ginger.museum.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ginger.museum.R;

public class QuizFragment extends BaseFragment {
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_quiz, container, false);
        initTopBar(rootView);
        return rootView;
    }

    @Override
    public String getTitle() {
        return getString(R.string.screen_title_quiz);
    }
}
