package com.ginger.museum.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ginger.museum.R;
import com.ginger.museum.model.Home;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;

public class HomeItem2Fragment extends BaseFragment {
	
	Home homeItem;
	TextView textTitle;
	TextView textSubTitle;
	TextView textDescription;
	ImageView image;
	View rootView = null;

	public Home getHomeItem() {
		return homeItem;
	}
	public void setHomeItem(Home homeItem) {
		this.homeItem = homeItem;
	}

	public HomeItem2Fragment() {
		
	}
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_home_item2, container, false);
			textTitle = (TextView) rootView.findViewById(R.id.textTitle);
			textSubTitle = (TextView) rootView.findViewById(R.id.textSubTitle);
			textDescription = (TextView) rootView.findViewById(R.id.textDescription);
			image = (ImageView)rootView.findViewById(R.id.image);
		}
        return rootView;
    }

	@Override
	public void reloadData() {
		super.reloadData();
		if (homeItem != null) {
			if (textTitle != null) {
				textTitle.setText(homeItem.getTitle());
				textSubTitle.setText(homeItem.getSubTitle());
				textDescription.setText(homeItem.getDescription());
				LogUtils.d(getClass().toString(), homeItem.getImage().getUrl());
				ImageUtils.displayImage(getActivity(), homeItem.getImage().getUrl(), image);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		reloadData();
	}

	@Override
	public long getID() {
		long itemId =  super.getID();
		if (homeItem != null) {
		}
		return itemId;
	}
}
