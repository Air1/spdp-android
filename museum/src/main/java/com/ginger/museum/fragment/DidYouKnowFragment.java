package com.ginger.museum.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.ginger.museum.PlayVideoActivity;
import com.ginger.museum.R;
import com.ginger.museum.model.DidYouKnow;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;

public class DidYouKnowFragment extends DialogFragment {

    DidYouKnow didYouKnow;
    View rootView;
    TextView titleView;
    TextView descriptionView;
    ImageView imageCharView;
    Context mContext;
    public DidYouKnowFragment() {
        // Required empty public constructor
    }

    public DidYouKnow getDidYouKnow() {
        return didYouKnow;
    }

    public void setDidYouKnow(DidYouKnow didYouKnow) {
        this.didYouKnow = didYouKnow;
    }
    /** The system calls this only when creating the layout in a dialog. */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // The only reason you might override this method when using onCreateView() is
        // to modify any dialog characteristics. For example, the dialog includes a
        // title by default, but your custom layout might not need it. So here you can
        // remove the dialog title, but you must call the superclass to get the Dialog.
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.full_screen_dialog);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_did_you_know, container, false);
            rootView.findViewById(R.id.buttonClose).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });
            rootView.findViewById(R.id.buttonPlayView).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    playMedia();
                }
            });
            titleView = (TextView)rootView.findViewById(R.id.textTitle);
            descriptionView = (TextView)rootView.findViewById(R.id.textDescription);
            imageCharView = (ImageView)rootView.findViewById(R.id.imageChar);
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (didYouKnow != null) {
            for (String key : didYouKnow.keySet()) {
                LogUtils.d(getClass().toString(), key + " : " + didYouKnow.get(key) + "");
            }
            titleView.setText(didYouKnow.getTitle());
            descriptionView.setText(didYouKnow.getDescription());
            if (didYouKnow.getImage() != null) {
                ImageUtils.displayImage(mContext, didYouKnow.getImage().getUrl(), imageCharView);
            }
            if (didYouKnow.getLink() != null && didYouKnow.getLink().length() > 0) {
                LogUtils.d(getClass().toString(), didYouKnow.getLink());
                rootView.findViewById(R.id.buttonPlayView).setVisibility(View.VISIBLE);
            } else {
                rootView.findViewById(R.id.buttonPlayView).setVisibility(View.INVISIBLE);
            }
        }
    }
    /*
     *
     */
    void playMedia() {
        Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
        intent.putExtra(PlayVideoActivity.LINK, didYouKnow.getLink());
        startActivity(intent);
    }
}
