package com.ginger.museum.fragment;

import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ginger.museum.R;
import com.ginger.museum.model.Exhibition;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;
import com.ginger.museum.utils.UiUtils;
import com.ginger.museup.ui.CustomHorizontalScrollView;
import com.ginger.museup.ui.OnSwipeTouchListener;

public class ExhibitLandingSection3Fragment extends BaseFragment {
	private static final int ANIMATION_TIME = 1000;
	View rootView;
	ImageView section3Artwork;
	ImageView imagePinPlus;
	boolean isShowingSectionArtwork = false;
	TextView section3Description;
	TextView section3Title;
	Exhibition exhibitionItem;
	int leftMargin = 0;

	int screenWidth;
	int screenHeight;
	public Exhibition getExhibitionItem() {
		return exhibitionItem;
	}

	public void setExhibitionItem(Exhibition exhibitionItem) {
		this.exhibitionItem = exhibitionItem;
	}

	boolean isShowAnimation = false;

	public boolean isShowAnimation() {
		return isShowAnimation;
	}

	public void setIsShowAnimation(boolean isShowAnimation) {
		this.isShowAnimation = isShowAnimation;
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
	        rootView = inflater.inflate(R.layout.fragment_exhibition_section3, container, false);
	        section3Artwork = (ImageView)rootView.findViewById(R.id.section3Artwork);
			imagePinPlus = (ImageView)rootView.findViewById(R.id.imagePinPlus);
	        //set event click
			section3Artwork.setOnClickListener(onClickListener);
			imagePinPlus.setOnClickListener(onClickListener);
			section3Artwork.setOnTouchListener(new OnSwipeTouchListener(mContext) {
				@Override
				public void onSwipeLeft() {
					hideArtwork();
				}

				@Override
				public void onSwipeRight() {

				}

				@Override
				public void onTap() {
					if (!isShowingSectionArtwork) {
						showArtwork();
					} else {
						if (onNavigationListener != null) {
							onNavigationListener.showArtworkDetail(exhibitionItem.getSection3Artwork());
						}
					}
				}
			});
	        //define
			section3Title = (TextView)rootView.findViewById(R.id.section3Title);
	        section3Description = (TextView) rootView.findViewById(R.id.section3Description);
	        isShowingSectionArtwork = false;
			rootView.findViewById(R.id.buttonDidYouKnow).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (onNavigationListener != null) {
						onNavigationListener.showDidYouKnow(exhibitionItem.getSection3DidYouKnow());
					}
				}
			});
			screenWidth = UiUtils.getScreenWidth(mContext);
		}
        return rootView;
    }
	@Override
	public void onResume() {
		super.onResume();
		reloadData();
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			if (!isShowingSectionArtwork) {
				showArtwork();
			} else {
				if (onNavigationListener != null) {
					onNavigationListener.showArtworkDetail(exhibitionItem.getSection3Artwork());
				}
			}
		}
	};

	@Override
	public void reloadData() {
		super.reloadData();
		if (exhibitionItem != null) {
			section3Title.setText(exhibitionItem.getSection3Title());
			section3Description.setText(exhibitionItem.getSection3Description());
			LogUtils.d(getClass().toString(), "section3Description: " + exhibitionItem.getSection3Description());
			if (exhibitionItem.getSection3Artwork() != null && exhibitionItem.getSection3Artwork().getImage() != null) {
				LogUtils.d("Section 3 Artwork Url:", exhibitionItem.getSection3Artwork().getImage().getUrl());
				ImageUtils.loadImageView(mContext,
						exhibitionItem.getSection3Artwork().getImage().getUrl(),
						section3Artwork, new ImageUtils.LoadCallBack() {
							@Override
							public void onLoadCompleted(Bitmap image) {
								if (getActivity() == null) {
									return;
								}
								RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams)section3Artwork.getLayoutParams();
								param.width = screenWidth;
								if (image != null) {

									if (image.getWidth() < screenWidth) {
										param.width = image.getWidth();
										param.height = image.getHeight();
									} else {
										float scaleX = screenWidth / 1080.0f;
										param.width = (int)(image.getWidth() * scaleX);
										param.height = (int)(image.getHeight() * scaleX);
										if (param.width > screenWidth) {
											param.width = screenWidth - getResources().getDimensionPixelSize(R.dimen.image_size_pinplus);
										}
									}
									param.height = (int)(param.width * (image.getHeight()*1.0f/image.getWidth()));
									LogUtils.d("Image size artwork 3:", param.height + "");
								}
								int availWidth = screenWidth - mContext.getResources().getDimensionPixelSize(R.dimen.s3_section3_margin_right) -  mContext.getResources().getDimensionPixelSize(R.dimen.s3_section_description_width) -mContext.getResources().getDimensionPixelSize(R.dimen.s3_section3_margin_image_text) ;
								leftMargin = param.width - availWidth;
								section3Artwork.setLayoutParams(param);
								if (isShowAnimation) {
									section3Artwork.setTranslationX(-leftMargin);
									imagePinPlus.setTranslationX(-leftMargin);
								} else {
									section3Artwork.setTranslationX(-leftMargin - 500);
									imagePinPlus.setTranslationX(-leftMargin - 500);
								}
								imagePinPlus.invalidate();
								rootView.findViewById(R.id.mainSection).invalidate();
								RelativeLayout.LayoutParams pr = (RelativeLayout.LayoutParams )imagePinPlus.getLayoutParams();
								pr.topMargin = (((RelativeLayout)imagePinPlus.getParent()).getHeight() - param.height)/2 +
										param.height - imagePinPlus.getHeight() - getResources().getDimensionPixelSize(R.dimen.s2_margin);
								imagePinPlus.setLayoutParams(pr);


							}
						});
			}
			if (exhibitionItem.getSection3DidYouKnow() != null) {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.VISIBLE);
			} else {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.INVISIBLE);
			}
			if (!isShowAnimation) {
				section3Description.setAlpha(0.0f);
			}
		}
	}
	/*
	 * 
	 */
	public void showAnimation() {
		//face in animation
		ObjectAnimator.ofFloat(section3Description, "alpha", 0.0f, 1.0f).setDuration(ANIMATION_TIME).start();
		//animation image

		ObjectAnimator.ofFloat(section3Artwork,
				"translationX", -leftMargin - 500, -leftMargin).setDuration(ANIMATION_TIME).start();
		ObjectAnimator.ofFloat(imagePinPlus,
				"translationX", -leftMargin - 500, -leftMargin).setDuration(ANIMATION_TIME).start();
	}
	int translateImageX;
	/*
	 * 
	 */
	public void showArtwork() {
		LogUtils.d("showArtwork", "showArtwork");
		isShowingSectionArtwork = true;
		int screenWidth = UiUtils.getScreenWidth(getActivity());
		//get position
		translateImageX = screenWidth/2 - section3Artwork.getWidth()/2 - section3Artwork.getLeft();
		ObjectAnimator.ofFloat(section3Artwork,
				"translationX", -leftMargin, translateImageX).setDuration(ANIMATION_TIME).start();
		ObjectAnimator.ofFloat(imagePinPlus,
				"translationX", -leftMargin, translateImageX).setDuration(ANIMATION_TIME).start();
	}
	/*
	 *
	 */
	void hideArtwork() {
		isShowingSectionArtwork = false;
		ObjectAnimator.ofFloat(section3Artwork,
				"translationX", translateImageX, -leftMargin).setDuration(ANIMATION_TIME).start();
		ObjectAnimator.ofFloat(imagePinPlus,
				"translationX", translateImageX, -leftMargin).setDuration(ANIMATION_TIME).start();
	}
	
}
