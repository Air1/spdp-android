package com.ginger.museum.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ginger.museum.R;
import com.ginger.museum.model.Artwork;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;

import java.util.Calendar;

public class ArtworkDetailFragment extends BaseFragment {

	View rootView;
	ImageView section1Artwork;
	boolean isShowingSectionArtwork = false;
	TextView section1Description;
	Artwork artworkItem;
	TextView favoriteButton;
	public Artwork getArtworkItem() {
		return artworkItem;
	}

	public void setArtworkItem(Artwork artworkItem) {
		this.artworkItem = artworkItem;
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
	        rootView = inflater.inflate(R.layout.fragment_artwork_detail, container, false);
			favoriteButton = (TextView)rootView.findViewById(R.id.buttonAddToFavorite);
			favoriteButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (artworkItem.isFavorite(mContext)) {
						artworkItem.setFavorite(mContext, false);
						favoriteButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.heart_fav_not_added), null);
						favoriteButton.setText("Add to favorites");
					} else {
						artworkItem.setFavorite(mContext, true);
						favoriteButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.heart_fav_added), null);
						favoriteButton.setText("Added to favorites");
					}

				}
			});
			initTopBar(rootView);
			rootView.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					getFragmentManager().popBackStack();
				}
			});
			rootView.findViewById(R.id.buttonDidYouKnow).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (onNavigationListener != null) {
						onNavigationListener.showDidYouKnow(artworkItem.getDidYouKnow());
					}
				}
			});
		}
        return rootView;
    }

	@Override
	public void onResume() {
		super.onResume();
		reloadData();
	}

	@Override
	public void reloadData() {
		super.reloadData();
		if (artworkItem != null) {
			LogUtils.d("Artwork ID", artworkItem.getObjectId());
			for (String key : artworkItem.keySet()) {
				LogUtils.d(getClass().toString(), key + " : " + artworkItem.get(key) + "");
			}
			if (artworkItem.getImage() != null){
				LogUtils.d(getClass().toString(), "reload Image: " + artworkItem.getImage().getUrl());
				ImageUtils.displayImage(mContext, artworkItem.getImage().getUrl(), (ImageView) rootView.findViewById(R.id.image));
			}
			if (artworkItem.isCuratorChoice()) {
				rootView.findViewById(R.id.buttonCurator).setVisibility(View.VISIBLE);
			} else {
				rootView.findViewById(R.id.buttonCurator).setVisibility(View.INVISIBLE);
			}
			((TextView)rootView.findViewById(R.id.textTitle)).setText(artworkItem.getTitle());
			((TextView)rootView.findViewById(R.id.textSubTitle)).setText(artworkItem.getDate());
			((TextView)rootView.findViewById(R.id.textDescription)).setText(artworkItem.getDescription());
			if (artworkItem.isFavorite(mContext)) {
				favoriteButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.heart_fav_added), null);
				favoriteButton.setText("Added to favorites");
			} else {
				favoriteButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.heart_fav_not_added), null);
				favoriteButton.setText("Add to favorites");
			}
			if (artworkItem.getDidYouKnow() != null) {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.VISIBLE);
			} else {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.INVISIBLE);
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		LogUtils.d("onDestroy", "Test onDestroy");
	}
	@Override
	public long getID() {
		return Calendar.getInstance().getTimeInMillis();
	}

}
















































































