package com.ginger.museum.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ginger.museum.R;
import com.ginger.museum.model.Exhibition;
import com.ginger.museum.model.Home;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;

public class ExhibitionItemFragment extends BaseFragment {

	View rootView;
	Exhibition exhibitionItem;
	ExhibitionNavigation onExhibitionNavigationListener;

	public Exhibition getExhibitionItem() {
		return exhibitionItem;
	}

	public void setExhibitionItem(Exhibition exhibitionItem) {
		this.exhibitionItem = exhibitionItem;
	}

	public ExhibitionNavigation getOnExhibitionNavigationListener() {
		return onExhibitionNavigationListener;
	}

	public void setOnExhibitionNavigationListener(ExhibitionNavigation onExhibitionNavigationListener) {
		this.onExhibitionNavigationListener = onExhibitionNavigationListener;
	}

	public ExhibitionItemFragment() {
		
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_exhibition_item, container, false);
			rootView.findViewById(R.id.buttonMoreDetail).setOnClickListener(onClickEvent);
			rootView.findViewById(R.id.buttonDidYouKnow).setOnClickListener(onClickEvent);
		}
        return rootView;
    }

	/*

	 */
	View.OnClickListener onClickEvent = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			switch (view.getId()) {
				case R.id.buttonMoreDetail:
					if (onExhibitionNavigationListener != null) {
						onExhibitionNavigationListener.onNavigateToExhibitionLandingPage1(exhibitionItem);
					}
					break;
				case R.id.buttonDidYouKnow:
					if (onNavigationListener != null) {
						onNavigationListener.showDidYouKnow(exhibitionItem.getDidYouKnow());
					}
					break;
				default:
					break;
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		reloadData();
	}

	@Override
	public void reloadData() {
		super.reloadData();
		LogUtils.d(getClass().toString(), "set data for exhibition card");
		if (exhibitionItem != null) {
			if (exhibitionItem.isTemporary()) {
				rootView.findViewById(R.id.textFeature).setVisibility(View.VISIBLE);
			} else {
				rootView.findViewById(R.id.textFeature).setVisibility(View.INVISIBLE);
			}
			((TextView)rootView.findViewById(R.id.textTitle)).setText(exhibitionItem.getName());
			((TextView)rootView.findViewById(R.id.textSubTitle)).setText(exhibitionItem.getDates());
			((TextView)rootView.findViewById(R.id.textDescription)).setText(exhibitionItem.getCardDescription());
			if (exhibitionItem.getDidYouKnow() != null) {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.VISIBLE);
			} else {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.INVISIBLE);
			}
			ImageUtils.displayImage(mContext, exhibitionItem.getCover().getUrl(),
					(ImageView) rootView.findViewById(R.id.imageCover));
			//show Sponsored by
			if (exhibitionItem.getSponsors() != null) {
				rootView.findViewById(R.id.listSponsors).setVisibility(View.VISIBLE);
				rootView.findViewById(R.id.textSponsored).setVisibility(View.VISIBLE);
				LogUtils.d("Sponser Image", "URL:" + exhibitionItem.getSponsors().getUrl());
				ImageUtils.displayImage(mContext, exhibitionItem.getSponsors().getUrl(),
						(ImageView) rootView.findViewById(R.id.sponsors1));
			} else {
				rootView.findViewById(R.id.listSponsors).setVisibility(View.INVISIBLE);
				rootView.findViewById(R.id.textSponsored).setVisibility(View.INVISIBLE);

			}

		}
	}
	/*
	 *
	 */
	public static interface ExhibitionNavigation {
		void onNavigateToExhibitionLandingPage1(Exhibition exhibition);
	}
}
