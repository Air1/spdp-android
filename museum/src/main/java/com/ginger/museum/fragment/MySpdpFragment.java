package com.ginger.museum.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.etsy.android.grid.StaggeredGridView;
import com.ginger.museum.R;
import com.ginger.museum.adapter.ArtworksAdapter;
import com.ginger.museum.model.Artwork;
import com.ginger.museum.model.DataProvider;
import com.parse.FindCallback;
import com.parse.ParseException;

import java.util.ArrayList;
import java.util.List;


public class MySpdpFragment extends BaseFragment {

    ArtworksAdapter adapter = null;
    List<Artwork> listArtworks;
    TextView textTitleResult;
    View rootView;
    public MySpdpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_myspdp, container, false);
            StaggeredGridView gridView = (StaggeredGridView) rootView.findViewById(R.id.listResult);
            listArtworks = new ArrayList<Artwork>();
            adapter = new ArtworksAdapter(mContext, listArtworks);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    if (onNavigationListener != null) {
                        onNavigationListener.showArtworkDetail(listArtworks.get(i));
                    }
                }
            });
            textTitleResult = (TextView)rootView.findViewById(R.id.textTitleResult);
            textTitleResult.setText("");
            initTopBar(rootView);
        }
        return rootView;
    }
    @Override
    public String getTitle() {
        return getString(R.string.screen_title_myspdp);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onNavigationListener = (OnNavigationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadData();
    }

    @Override
    public void reloadData() {
        super.reloadData();
        DataProvider.getInstance().fetchArtworkRecords(new FindCallback<Artwork>() {
            @Override
            public void done(List<Artwork> list, ParseException e) {
                listArtworks.clear();
                if (list != null) {
                    for (Artwork artwork : list) {
                        if (artwork.isFavorite(mContext)) {
                            listArtworks.add(artwork);
                        }
                    }
                }
                textTitleResult.setText(listArtworks.size() + " Artworks");
                adapter.notifyDataSetChanged();
            }
        });
    }
}
