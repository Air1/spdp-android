package com.ginger.museum.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ginger.museum.R;
import com.ginger.museum.utils.LogUtils;

public class WebBrowserFragment extends BaseFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_LINK = "link";
    private String mLink;
    View rootView;
    WebView webView;
    public static WebBrowserFragment newInstance(String link) {
        WebBrowserFragment fragment = new WebBrowserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_LINK, link);
        fragment.setArguments(args);
        return fragment;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String mLink) {
        this.mLink = mLink;
    }

    public WebBrowserFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mLink = getArguments().getString(ARG_LINK);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_web_browser, container, false);
            webView = (WebView)rootView.findViewById(R.id.webView);
            webView.setWebViewClient(new MyBrowser());
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadData();
    }

    @Override
    public void reloadData() {
        super.reloadData();
        LogUtils.d(getClass().toString(), "Load link:" + mLink);
        if (mLink != null && mLink.length() > 0) {
            if (mLink.indexOf("http") == -1) {
                mLink = "http://" + mLink;
            }
            webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webView.loadUrl(mLink);
        }
    }
    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
