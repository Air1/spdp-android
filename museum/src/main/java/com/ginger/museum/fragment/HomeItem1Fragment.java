package com.ginger.museum.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ginger.museum.R;
import com.ginger.museum.model.Home;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;
import com.ginger.museum.utils.UiUtils;

public class HomeItem1Fragment extends BaseFragment {
	
	HomeItem1Listener listenner;
	Home homeItem;
	View rootView;
	/*
	 * 
	 */
	public HomeItem1Listener getListenner() {
		return listenner;
	}
	/*
	 * 
	 */
	public void setListenner(HomeItem1Listener listenner) {
		this.listenner = listenner;
	}

	public Home getHomeItem() {
		return homeItem;
	}

	public void setHomeItem(Home homeItem) {
		this.homeItem = homeItem;
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_home_item1, container, false);
			rootView.findViewById(R.id.buttonFeatures).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (listenner != null) {
						listenner.onFeaturesClicked();
					}
				}
			});
			rootView.findViewById(R.id.buttonCollections).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (listenner != null) {
						listenner.onCollectionsClicked();
					}
				}
			});
			rootView.findViewById(R.id.buttonGallery).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (listenner != null) {
						listenner.onGalerryClicked();
					}
				}
			});
		}
        return rootView;
    }

	@Override
	public void onResume() {
		super.onResume();
		reloadData();
	}

	@Override
	public void reloadData() {
		super.reloadData();
		if (homeItem != null) {
			LogUtils.d("Main Image URL:", homeItem.getImage().getUrl());
			final ImageView mainCard = (ImageView)rootView.findViewById(R.id.mainImage);
//			Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.frame);
//			ImageUtils.displayBackgroundImage(mContext, homeItem.getImage().getUrl(), mainCard, bitmap.getWidth(), bitmap.getHeight());
			ImageUtils.loadImageView(mContext, homeItem.getImage().getUrl(), mainCard, new ImageUtils.LoadCallBack() {
				@Override
				public void onLoadCompleted(Bitmap image) {
					if (image != null) {
//						int screenWidth = UiUtils.getScreenWidth(mContext);
//						int screenHeight = UiUtils.getScreenHeight(mContext);
//						LogUtils.d("Scale size", screenWidth + "/" + screenHeight);
//						image = Bitmap.createScaledBitmap(image, screenWidth, screenHeight, false);
//						mainCard.setImageBitmap(image);
					}
				}
			});
		}
	}

	/*
             *
             */
	public interface HomeItem1Listener {
		void onFeaturesClicked();
		void onCollectionsClicked();
		void onGalerryClicked();
	}
}
