package com.ginger.museum.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.ginger.museum.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends BaseFragment {

    Button buttonLevel1;
    Button buttonLevel2;
    Button buttonBasement;
    ImageView imgageMap;
    public MapFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        buttonLevel1 = (Button)rootView.findViewById(R.id.buttonLevel1);
        buttonLevel2 = (Button)rootView.findViewById(R.id.buttonLevel2);
        buttonBasement = (Button)rootView.findViewById(R.id.buttonBasement);
        imgageMap = (ImageView)rootView.findViewById(R.id.imgageMap);
        buttonLevel1.setOnClickListener(onClickedEvent);
        buttonLevel2.setOnClickListener(onClickedEvent);
        buttonBasement.setOnClickListener(onClickedEvent);
        initTopBar(rootView);
        return rootView;
    }
    View.OnClickListener onClickedEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            buttonLevel1.setBackgroundResource(R.drawable.lv1_off);
            buttonLevel2.setBackgroundResource(R.drawable.lv1_off);
            buttonBasement.setBackgroundResource(R.drawable._basement_off);
            buttonLevel1.setTextColor(getResources().getColor(R.color.pink_color));
            buttonLevel2.setTextColor(getResources().getColor(R.color.pink_color));
            buttonBasement.setTextColor(getResources().getColor(R.color.pink_color));
            switch (v.getId()) {
                case R.id.buttonLevel1:
                    buttonLevel1.setBackgroundResource(R.drawable.lv1_on);
                    buttonLevel1.setTextColor(getResources().getColor(android.R.color.white));
                    imgageMap.setImageResource(R.drawable.map01);
                    break;
                case R.id.buttonLevel2:
                    buttonLevel2.setBackgroundResource(R.drawable.lv1_on);
                    buttonLevel2.setTextColor(getResources().getColor(android.R.color.white));
                    imgageMap.setImageResource(R.drawable.map02);
                    break;
                case R.id.buttonBasement:
                    buttonBasement.setBackgroundResource(R.drawable._basement_on);
                    buttonBasement.setTextColor(getResources().getColor(android.R.color.white));
                    imgageMap.setImageResource(R.drawable.map03);
                    break;
            }
        }
    };
    @Override
    public String getTitle() {
        return getString(R.string.screen_title_map);
    }

}
