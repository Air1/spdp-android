package com.ginger.museum.fragment;

import java.util.ArrayList;
import java.util.List;

import com.ginger.museum.R;
import com.ginger.museum.adapter.BaseFragmentPagerAdapter;
import com.ginger.museum.fragment.HomeItem1Fragment.HomeItem1Listener;
import com.ginger.museum.model.DataProvider;
import com.ginger.museum.model.Home;
import com.ginger.museum.utils.LogUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.viewpagerindicator.CirclePageIndicator;
import android.app.FragmentTransaction;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;


public class HomeFragment extends BaseFragment implements HomeItem1Listener {
	
	ViewPager pagerHomeCard;
	List<BaseFragment> cards;
	View rootView;
	BaseFragmentPagerAdapter pagerAdapter;
	public HomeFragment() {
		
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
	        rootView = inflater.inflate(R.layout.fragment_home, container, false);
	        pagerHomeCard = (ViewPager) rootView.findViewById(R.id.pagerHomeCard);
			//init first card
			if (cards == null) {
				cards = new ArrayList<BaseFragment>();
				pagerAdapter = new BaseFragmentPagerAdapter(getFragmentManager(), cards);
				pagerHomeCard.setAdapter(pagerAdapter);
			}
	        reloadData();
	        //set indicator
	        CirclePageIndicator indicator = (CirclePageIndicator) rootView.findViewById(R.id.indicator);
	        indicator.setViewPager(pagerHomeCard);
	        indicator.setPageColor(getResources().getColor(R.color.pager_color));
	        indicator.setFillColor(getResources().getColor(R.color.pager_color_fill));
	        indicator.setStrokeColor(getResources().getColor(R.color.pager_color_fill));
	        indicator.setStrokeWidth(0);
	        indicator.setRadius(BitmapFactory.decodeResource(getResources(), R.drawable._dot_active).getWidth() / 2);
			initTopBar(rootView);
			//reset topbar
			rootView.findViewById(R.id.screenTitle).setVisibility(View.GONE);
			View buttonHome = rootView.findViewById(R.id.buttonHome);
			RelativeLayout parent = (RelativeLayout)buttonHome.getParent();
			RelativeLayout.LayoutParams pr = (RelativeLayout.LayoutParams)buttonHome.getLayoutParams();
			pr.addRule(RelativeLayout.CENTER_HORIZONTAL);
			buttonHome.setLayoutParams(pr);

		}
        return rootView;
    }

	@Override
	public void reloadData() {
		super.reloadData();
		//The first card is always a cards is hardcoded data

		//get data from table Home
		DataProvider.getInstance().fetchHomeRecords(new FindCallback<Home>() {
			@Override
			public void done(List<Home> list, ParseException e) {
				//clear all except first card
				if (list != null && list.size() > 0) {
					HomeItem1Fragment firstCard;
					if (cards.size() == 0) {
						firstCard = new HomeItem1Fragment();
						firstCard.setListenner(HomeFragment.this);
						cards.add(firstCard);
					} else {
						firstCard = (HomeItem1Fragment)cards.get(0);
					}
					firstCard.setHomeItem(list.get(0));
					//update
					for (int i = 1; i < cards.size() && i < list.size(); i++) {
						HomeItem2Fragment card = (HomeItem2Fragment)cards.get(i);
						card.setHomeItem(list.get(i));
						card.reloadData();
					}
					//delete cards
					for (int i = cards.size()-1; i >= list.size(); i--) {
						cards.remove(i);
					}
					//update new card
					for (int i = cards.size(); i < list.size(); i++) {
						HomeItem2Fragment card2 = new HomeItem2Fragment();
						card2.setHomeItem(list.get(i));
						cards.add(card2);
					}
					pagerAdapter.notifyDataSetChanged();
				}
			}
		});
	}

	@Override
	public void onFeaturesClicked() {
		if (onNavigationListener != null) {
			onNavigationListener.showExhibitionsHome(0);
		}
	}

	@Override
	public void onCollectionsClicked() {
		if (onNavigationListener != null) {
			onNavigationListener.showExhibitionsHome(1);
		}
	}

	@Override
	public void onGalerryClicked() {
		if (onNavigationListener != null) {
			onNavigationListener.showExhibitionsHome(2);
		}
	}
}
