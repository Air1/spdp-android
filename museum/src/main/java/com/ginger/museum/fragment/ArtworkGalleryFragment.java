package com.ginger.museum.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.etsy.android.grid.StaggeredGridView;
import com.ginger.museum.R;
import com.ginger.museum.adapter.ArtworksAdapter;
import com.ginger.museum.adapter.GalleryAdapter;
import com.ginger.museum.model.Artwork;
import com.ginger.museum.model.DataProvider;
import com.ginger.museum.model.Exhibition;
import com.ginger.museup.ui.CustomTextView;
import com.parse.FindCallback;
import com.parse.ParseException;

import java.util.ArrayList;
import java.util.List;


public class ArtworkGalleryFragment extends BaseFragment {

    GalleryAdapter adapter = null;
    List<Artwork> listArtworks;
    View rootView;
    Exhibition exhibitionItem;
    OnGalleryEvent onGalleryEvent;
    public ArtworkGalleryFragment() {
        // Required empty public constructor
    }

    public Exhibition getExhibitionItem() {
        return exhibitionItem;
    }

    public void setExhibitionItem(Exhibition exhibitionItem) {
        this.exhibitionItem = exhibitionItem;
    }

    public void setOnGalleryEvent(OnGalleryEvent onGalleryEvent) {
        this.onGalleryEvent = onGalleryEvent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_artwork_galerry, container, false);
            ListView listView = (ListView) rootView.findViewById(R.id.listArtworks);
            listArtworks = new ArrayList<Artwork>();
            adapter = new GalleryAdapter(mContext, listArtworks);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    if (onGalleryEvent != null) {
                        onGalleryEvent.onItemArtworkClicked(listArtworks.get(i));
                    }
                }
            });
            reloadData();
            rootView.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onGalleryEvent != null) {
                        onGalleryEvent.onBack();
                    }
                }
            });
        }
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onNavigationListener = (OnNavigationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void reloadData() {
        super.reloadData();
        if (exhibitionItem != null) {
            DataProvider.getInstance().searchArtwork("", exhibitionItem.getObjectId(), new FindCallback<Artwork>() {
                @Override
                public void done(List<Artwork> list, ParseException e) {
                    if (list != null) {
                        listArtworks.clear();
                        listArtworks.addAll(list);
                        adapter.notifyDataSetChanged();
                    }
                }
            });
        }
    }
    public interface  OnGalleryEvent {
        void onItemArtworkClicked (Artwork artwork);
        void onBack();
    }
}
