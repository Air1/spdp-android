package com.ginger.museum.fragment;

import java.util.ArrayList;
import java.util.List;

import com.ginger.museum.R;
import com.ginger.museum.adapter.BaseFragmentPagerAdapter;
import com.ginger.museum.model.DataProvider;
import com.ginger.museum.model.Exhibition;
import com.ginger.museum.utils.LogUtils;
import com.ginger.museup.ui.CustomScrollView;
import com.ginger.museup.ui.CustomScrollView.OnScrollViewListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.viewpagerindicator.CirclePageIndicator;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


public class ExhibitionHomeFragment extends BaseFragment
		implements ExhibitionItemFragment.ExhibitionNavigation {

	public final static String EXHIBITION_CURRENT_ITEM = "EXHIBITION:ITEM";
	ViewPager pagerHomeCard;
	List<BaseFragment> cards;
	BaseFragmentPagerAdapter adapter;

	View rootView;
	int currentItem = 0;
	int itemHeight;
	CustomScrollView mainScrollView;

	public ExhibitionHomeFragment() {
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_exhibition_home, container, false);
			pagerHomeCard = (ViewPager) rootView.findViewById(R.id.pagerExhibitionHomeCard);
	        pagerHomeCard.setOffscreenPageLimit(3);
			cards = new ArrayList<BaseFragment>();
			adapter = new BaseFragmentPagerAdapter(getFragmentManager(), cards);
			pagerHomeCard.setAdapter(adapter);


	        //set indicator
	        CirclePageIndicator indicator = (CirclePageIndicator) rootView.findViewById(R.id.indicator);
	        indicator.setViewPager(pagerHomeCard);
	        indicator.setPageColor(getResources().getColor(R.color.pager_color));
	        indicator.setFillColor(getResources().getColor(R.color.pager_color_fill));
	        indicator.setStrokeColor(getResources().getColor(R.color.pager_color_fill));
	        indicator.setStrokeWidth(0);
			indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
				@Override
				public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

				}

				@Override
				public void onPageSelected(int position) {
//					currentItem = position;
				}

				@Override
				public void onPageScrollStateChanged(int state) {

				}
			});
			mainScrollView = (CustomScrollView) rootView.findViewById(R.id.mainScrollView);
//			mainScrollView.setEnabled(false);
			mainScrollView.setOnScrollViewListener(new OnScrollViewListener() {
				@Override
				public void onScrollChanged(CustomScrollView v, int l, int t, int oldl,
											int oldt) {
				}

				@Override
				public void onScrollTop() {
				}

				@Override
				public void onScrollDown() {
					Log.d("onScrollDown", "On ScrollDown");
					if (cards.size() > 0) {
						ExhibitionItemFragment currentItem = (ExhibitionItemFragment) cards.get(pagerHomeCard.getCurrentItem());
						showNextPage(currentItem.getExhibitionItem());
					}
				}

			});
			initTopBar(rootView);
		}
        return rootView;
    }

	@Override
	public String getTitle() {
		return getString(R.string.screen_title_exhibitions);
	}

	@Override
	public void onResume() {
		super.onResume();
		reloadData();
		 //set height
		rootView.findViewById(R.id.exhibitionHome).getLayoutParams().height = itemHeight;
		rootView.findViewById(R.id.temp).getLayoutParams().height = itemHeight;
		mainScrollView.smoothScrollTo(0, 0);
//		rootView.findViewById(R.id.mainLayout1).getLayoutParams().height = itemHeight + itemHeight;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
	}
	
	@Override
	public void setArguments(Bundle args) {
		super.setArguments(args);
	}
	/*
	 * 
	 */

	@Override
	public void reloadData() {
		super.reloadData();
		LogUtils.d(getClass().toString(), "Reload data on Exhibition Screen");
		DataProvider.getInstance().fetchExhibitionRecords(new FindCallback<Exhibition>() {
			@Override
			public void done(List<Exhibition> list, ParseException e) {
				if (list != null && list.size() > 0) {
					cards.clear();
					for (Exhibition ex : list) {
						ExhibitionItemFragment fragment = new ExhibitionItemFragment();
						fragment.setExhibitionItem(ex);
						fragment.setOnNavigationListener(onNavigationListener);
						fragment.setOnExhibitionNavigationListener(ExhibitionHomeFragment.this);
						cards.add(fragment);
					}
					adapter.notifyDataSetChanged();
				}
				pagerHomeCard.postDelayed(new Runnable() {
					@Override
					public void run() {
						LogUtils.d(getClass().toString(), "Reset current item" + currentItem);
						pagerHomeCard.setCurrentItem(currentItem, true);
					}
				}, 500);
				LogUtils.d(getClass().toString(), "Reset current item" + currentItem);
				pagerHomeCard.setCurrentItem(currentItem, true);
				//set current fragment
			}
		});
	}

	/*
	 * 
	 */
	public void setCurrentItem(int _currentItem) {
		LogUtils.d(getClass().toString(), "Reset current item" + _currentItem);
		if (_currentItem != -1) {
			currentItem = _currentItem;
			if (pagerHomeCard != null) {
				pagerHomeCard.setCurrentItem(currentItem);
			}
		}
	}
	/*
	 * 
	 */
	public void setItemHeight(int height) {
		itemHeight = height;
	}
	/*
	 * 
	 */
	public void showNextPage(Exhibition exhibition) {
		mainScrollView.smoothScrollTo(0, 0);
		onNavigationListener.showExhibitionDetail(pagerHomeCard.getCurrentItem(), exhibition, itemHeight);
	}
	@Override
	public void onNavigateToExhibitionLandingPage1(Exhibition exhibition) {
		showNextPage(exhibition);
	}
}
