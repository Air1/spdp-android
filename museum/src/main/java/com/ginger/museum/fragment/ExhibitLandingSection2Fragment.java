package com.ginger.museum.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.ginger.museum.R;
import com.ginger.museum.model.Exhibition;
import com.ginger.museum.utils.ImageUtils;
import com.ginger.museum.utils.LogUtils;
import android.graphics.Bitmap;

public class ExhibitLandingSection2Fragment extends BaseFragment {
	
	View rootView;
	ImageView section2Artwork;
	Exhibition exhibitionItem;

	public Exhibition getExhibitionItem() {
		return exhibitionItem;
	}

	public void setExhibitionItem(Exhibition exhibitionItem) {
		this.exhibitionItem = exhibitionItem;
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (rootView == null) {
	        rootView = inflater.inflate(R.layout.fragment_exhibition_section2, container, false);
	        section2Artwork = (ImageView)rootView.findViewById(R.id.section2Artwork);
	        section2Artwork.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (onNavigationListener != null) {
						onNavigationListener.showArtworkDetail(exhibitionItem.getSection2Artwork());
					}
				}
			});
			rootView.findViewById(R.id.buttonDidYouKnow).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (onNavigationListener != null) {
						onNavigationListener.showDidYouKnow(exhibitionItem.getSection1DidYouKnow());
					}
				}
			});
		}
        return rootView;
    }
	@Override
	public void onResume() {
		super.onResume();
		reloadData();
	}

	@Override
	public void reloadData() {
		super.reloadData();
		if (exhibitionItem != null) {
			if (exhibitionItem.getSection2Artwork() != null && exhibitionItem.getSection2Artwork().getImage() != null) {
				LogUtils.d("Url", exhibitionItem.getSection2Artwork().getImage().getUrl());
				ImageUtils.loadImageView(mContext,
						exhibitionItem.getSection2Artwork().getImage().getUrl(),
						section2Artwork, new ImageUtils.LoadCallBack() {
							@Override
							public void onLoadCompleted(Bitmap image) {
							}
						});
			}
			if (exhibitionItem.getSection2DidYouKnow() != null) {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.VISIBLE);
			} else {
				rootView.findViewById(R.id.buttonDidYouKnow).setVisibility(View.INVISIBLE);
			}
		}
	}
}
